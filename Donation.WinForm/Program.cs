﻿using Donation.Models;
using Donation.Shared;
using Donation.WinForm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    static class Program
    {
        
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            CurrentForm = formVdo = new FormVdo();
            Timer.Interval = 1000;
            Timer.Tick += Timer_Tick;
            Timer.Start();

            Task.Run(() => Program.Rest.PiFront(new PiFrontParam
            {
                coincount = 0,
                coinbaht = 0,
                bankcount = 0,
                bankbaht = 0,
                total = 0

            }));
            //Task.Run(async () => var forReprot = await Program.DoDonate(200));
            Task.Run(() => Program.Rest.PiBack(new PiBackParam
            {
                coinbaht = 0,
                bankbaht = 0,
                total = 0
            }));

            formIdentity = new FormIdentity();
            formMoneyType = new FormMoneyType();
            formMoneyType2 = new FormMoneyType2();
            formOrganization    = new FormOrganization();
            formDonate          = new FormDonate();
            form11 = new Form11();
            form2 = new Form2();
            form8 = new Form8();
            formSearch = new FormSearch();
            formSearchItem = new FormSearchItem();
            formSearchOrDonation = new FormSearchOrDonation();
            formTransfer = new FormTransfer();
            formPrint = new FormPrint();
            formLetter = new FormLetter();
            formExit = new FormExit();
            
            Application.Run(formVdo);

        }
        static public ForReport ForReport { get; set; }
        static public async Task<ForReport> DoDonate(int baht)
        {
            var forReport = await Program.Rest.DonateMoney
                                         (Program.Token, baht.ToString()
                                          ,Program.Objective.ToString(), ""
                                          ,Program.HomeId.ToString()
                                          ,Global.ReceiptLetter?1:0
                                          ,Global.ThankYouLetter?1:0);
            if (forReport.status)
            {
                //Success;
            }
            else
            {

            }
            return forReport;
        }



        private static void Timer_Tick(object sender, EventArgs e)
        {
            if(DateTime.Now-LastChangePageTime > TimeSpan.FromMinutes(3) && CurrentForm != formVdo)
            {
                if (CurrentForm == Program.formDonate)
                {
                    ChangePageV2(CurrentForm, formVdo);
                }
                else {
                    ChangePage(CurrentForm, formVdo);
                }
                
            }
        }

        static Timer Timer { get; set; } = new Timer();
        static public DateTime LastChangePageTime { get; set; } = DateTime.Now;        static Form CurrentForm { get; set; }
        static public void ChangePage(Form _this,Form to)
        {
            try
            {

                to.Show();
                
                _this.Hide();
                if(to==Program.formVdo)
                {
                    Program.formVdo.StartVdo();
                }

                if (to == Program.formOrganization)
                {
                    Program.formOrganization.runAudio();
                }

                CurrentForm = to;
                RefreshTime();
            }
            catch(ObjectDisposedException)
            {

            }
            catch(Exception exc)
            {
                //throw;

            }
        }

        static public void ChangePageV2(Form _this, Form to)
        {
            try
            {
                to.Show();
                if (to==Program.formVdo)
                {
                    Program.formVdo.StartVdo();
                }
                if (to == Program.formOrganization)
                {
                    Program.formOrganization.runAudio();
                }
                _this.Close();
                CurrentForm = to;
                RefreshTime();
            }
            catch (ObjectDisposedException)
            {

            }
            catch (Exception exc)
            {
                //throw;
            }
        }
        static public void RefreshTime()
        {
            LastChangePageTime = DateTime.Now;
        }
        //Form
        static public FormIdentity formIdentity;
        static public FormMoneyType formMoneyType;
        static public FormMoneyType2 formMoneyType2;
        static public FormOrganization formOrganization;
        static public FormDonate formDonate;
        static public Form11 form11;
        static public Form2 form2;
        static public Form8 form8;
        static public FormSearch formSearch;
        static public FormSearchItem formSearchItem;
        static public FormVdo formVdo;
        static public FormSearchOrDonation formSearchOrDonation;
        static public FormTransfer formTransfer;
        static public FormPrint formPrint;
        static public FormLetter formLetter;
        static public FormExit formExit;

        //------------------------//
        static public ResponseForPrint ResponseForPrint { get; set; }
        static public List<HistoryItem> HistoryItemList { get; set; }
        static public Rest Rest { get; set; } = new Rest();
        static public int Objective { get; set; }
        static public int HomeId { get; set; }
        static public string AdminToken { get; set; }
        static public string KioskName { get; set; } = System.Configuration.ConfigurationManager.AppSettings["KioskName"];

        static public async Task AnonymousLogin(string kioskName)
        {
            bool success = false;
            do
            {
                var response = await Rest.GuestLogin(kioskName);
                if (response.status)
                {
                    Token = response.token;

                }
                else
                {
                    MessageBox.Show("กรุณา ต่ออินเทอร์เน็ต");
                }
                success = response.status;
            } while (!success);
        }
        static public string Token { get; set; }
    }
}
