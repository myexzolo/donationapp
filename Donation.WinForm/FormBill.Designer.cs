﻿namespace Donation.WinForm
{
    partial class FormBill
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labRecBook = new System.Windows.Forms.Label();
            this.labRec = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTxt = new System.Windows.Forms.Label();
            this.labName = new System.Windows.Forms.Label();
            this.labAmount = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Donation.WinForm.Properties.Resources.Thai_government;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(102, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(2, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "เล่มที่ ";
            this.label1.Visible = false;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(205, 3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 1;
            this.label2.Text = "เลขที่ ";
            this.label2.Visible = false;
            // 
            // labRecBook
            // 
            this.labRecBook.AutoSize = true;
            this.labRecBook.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRecBook.Location = new System.Drawing.Point(42, 3);
            this.labRecBook.Name = "labRecBook";
            this.labRecBook.Size = new System.Drawing.Size(33, 26);
            this.labRecBook.TabIndex = 1;
            this.labRecBook.Text = "162";
            this.labRecBook.Visible = false;
            // 
            // labRec
            // 
            this.labRec.AutoSize = true;
            this.labRec.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRec.Location = new System.Drawing.Point(247, 3);
            this.labRec.Name = "labRec";
            this.labRec.Size = new System.Drawing.Size(33, 26);
            this.labRec.TabIndex = 1;
            this.labRec.Text = "005";
            this.labRec.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(0, 80);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(280, 28);
            this.label5.TabIndex = 1;
            this.label5.Text = "ใบเสร็จรับเงินชั่วคราว";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 104);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(285, 28);
            this.label6.TabIndex = 1;
            this.label6.Text = "ในราชการกรมส่งเสริมและพัฒนาคุณภาพชีวิตคนพิการ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dateTxt
            // 
            this.dateTxt.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTxt.Location = new System.Drawing.Point(5, 138);
            this.dateTxt.Name = "dateTxt";
            this.dateTxt.Size = new System.Drawing.Size(275, 28);
            this.dateTxt.TabIndex = 1;
            this.dateTxt.Text = "วันที่ 09/10/2561";
            this.dateTxt.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labName
            // 
            this.labName.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labName.Location = new System.Drawing.Point(5, 176);
            this.labName.Name = "labName";
            this.labName.Size = new System.Drawing.Size(280, 28);
            this.labName.TabIndex = 2;
            this.labName.Text = "ได้รับเงินจาก ";
            this.labName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // labAmount
            // 
            this.labAmount.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labAmount.Location = new System.Drawing.Point(5, 232);
            this.labAmount.Name = "labAmount";
            this.labAmount.Size = new System.Drawing.Size(275, 28);
            this.labAmount.TabIndex = 3;
            this.labAmount.Text = "จำนวน";
            this.labAmount.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(5, 204);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(275, 28);
            this.label9.TabIndex = 1;
            this.label9.Text = "เป็นเงินบริจาคผ่านตู้รับบริจาคอัจฉริยะ";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Times New Roman", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1, 295);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(280, 28);
            this.label3.TabIndex = 1;
            this.label3.Text = "------------------------------------";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // FormBill
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(283, 344);
            this.Controls.Add(this.labAmount);
            this.Controls.Add(this.labName);
            this.Controls.Add(this.labRecBook);
            this.Controls.Add(this.labRec);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTxt);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBill";
            this.Text = "FormBill";
            this.Load += new System.EventHandler(this.FormBill_Load);
            this.Shown += new System.EventHandler(this.FormBill_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labRecBook;
        private System.Windows.Forms.Label labRec;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label dateTxt;
        private System.Windows.Forms.Label labName;
        private System.Windows.Forms.Label labAmount;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label3;
    }
}