﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
        }

        private void Donate()
        {
            Program.ChangePage(this, Program.formMoneyType);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NonthaburiMaleRacha"));
            thread.Start();
            Program.HomeId = 28;
            Donate();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NonthaburiFeMaleRacha"));
            thread.Start();
            Program.HomeId = 24;
            Donate();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NonthaburiNont"));
            thread.Start();
            Program.HomeId = 30;
            Donate();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NonthaburiFai"));
            thread.Start();
            Program.HomeId = 32;
            Donate();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("PathumThaniMale"));
            thread.Start();
            Program.HomeId = 14;
            Donate();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("PathumThaniFeMale"));
            thread.Start();
            Program.HomeId = 16;
            Donate();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("Ratchaburi"));
            thread.Start();
            Program.HomeId = 26;
            Donate();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("hugUbonRatchathani"));
            thread.Start();
            Program.HomeId = 20;
            Donate();
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("ChonBuriGarun"));
            thread.Start();
            Program.HomeId = 18;
            Donate();
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("ChachoengsaoBang"));
            thread.Start();
            Program.HomeId = 22;
            Donate();
        }

        private void pictureBox11_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("phapadang"));
            thread.Start();
            Program.HomeId = 36;
            Donate();
        }

        private void Form11_Load(object sender, EventArgs e)
        {
            displayScreens();
        }

        public void runAudio2(String s)
        {
            playWave(s);
        }


        public void runAudio()
        {
            playWave("selectDepartment");
        }

        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath);

        }


        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }

        private void Form11_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            Program.ChangePage(this, Program.formOrganization);
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formOrganization);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formOrganization);
        }
    }
}
