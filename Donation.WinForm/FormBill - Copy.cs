﻿using Donation.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormBill2 : Form
    {
        int amount = 0;
        public FormBill2()
        {
            InitializeComponent();

        }

   
        private void FormBill_Load(object sender, EventArgs e)
        {


        }

        public void SetParam(string dateBegin,string dateEnd,string total)
        {
            this.datePickerBegin.Text = "ตั้งแต่ วันที่ "+dateBegin;
            datePickerEnd.Text = "ถึง วันที่ "+dateEnd;
            labelTotal.Text = "รวมเป็นจำนวนเงิน "+total+" บาท";

        }

        void PrintImage(object o, PrintPageEventArgs e)
        {
            int width = this.Width;
            int height = this.Height;

            Graphics g1 = this.CreateGraphics();

            Rectangle bounds = new Rectangle(0, 0, width, height);
            Bitmap img = new Bitmap(width, height, g1);

            this.DrawToBitmap(img, bounds);
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.DrawImage(img, bounds, 0, 0, img.Width, img.Height, System.Drawing.GraphicsUnit.Pixel);
        }

        private void FormBill_Shown(object sender, EventArgs e)
        {
            this.Focus();
        /*    string departmentUnitName = "";
            string donateMoneyNote = "";
            Program.DoDonate(amount).ContinueWith(T => {
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {

                        labRecBook.Text = T.Result.moneyReceiptBookNumber;
                        labRec.Text = T.Result.moneyReceiptNumber;
                        departmentUnitName = T.Result.departmentUnitName;
                        donateMoneyNote = T.Result.donateMoneyNote;
                    }));
                }
                else
                {
                    labRecBook.Text = T.Result.moneyReceiptBookNumber;
                    labRec.Text = T.Result.moneyReceiptNumber;
                    departmentUnitName = T.Result.departmentUnitName;
                    donateMoneyNote = T.Result.donateMoneyNote;
                }
            });
            */
            PrintDocument print = new PrintDocument();
            print.PrintPage += new PrintPageEventHandler(PrintImage);
            print.Print();
            this.Close();

        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }
    }
}
