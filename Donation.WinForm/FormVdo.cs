﻿using Donation.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormVdo : Form
    {
        public bool flagActive = true; 
        public FormVdo()
        {
            InitializeComponent();
        }


        public async Task StartVdo()
        {
            try
            {
                var rest = new Rest();
                var response = await rest.GetPublicNewsList();
                if (response.status)
                {
                    var url = response.data.First().fileUrl;
                    this.Invoke(new MethodInvoker(() =>
                    {
                        
                        axWindowsMediaPlayer1.settings.setMode("loop", true);
                        axWindowsMediaPlayer1.uiMode = "none";
                        axWindowsMediaPlayer1.URL = url;
                       
                    }));
                        
                }
            }
            catch(Exception exc)
            {

            }
        }



        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                string RunningProcess = "Capture.exe";
                foreach (Process proc in Process.GetProcessesByName(RunningProcess))
                {
                    proc.Kill();
                }

                displayScreens();
                Task.Run(() => StartVdo());
            }
            catch(Exception exc)
            {

            }
        }

        private void FormVdo_Click(object sender, EventArgs e)
        {
            NextForm();
        }

        void NextForm()
        {
            //Program.ChangePage(this, Program.formIdentity);
            Program.formIdentity.Show();
            Program.formIdentity.runAudio();
            this.Hide();
            axWindowsMediaPlayer1.Ctlcontrols.stop();

        }

        private void label1_Click(object sender, EventArgs e)
        {
            NextForm();
        }

        private void FormVdo_Shown(object sender, EventArgs e)
        {
            //axWindowsMediaPlayer1.Ctlcontrols.play();
        }

        private void label1_Click_1(object sender, EventArgs e)
        {
            NextForm();
        }

        private void pictureBox2_DoubleClick_1(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Program.formExit.Show();
            this.Hide();
            axWindowsMediaPlayer1.Ctlcontrols.stop();
        }

        private void FormVdo_FormClosing(object sender, FormClosingEventArgs e)
        {
            string RunningProcess = Process.GetCurrentProcess().ProcessName;
            foreach (Process proc in Process.GetProcessesByName(RunningProcess))
            {
                proc.Kill();
            }  
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void panel2_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
