﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormOrganization : Form
    {
        private Thread runThread = null;
        public FormOrganization()
        {
            InitializeComponent();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("8"));
            thread.Start();
            Program.ChangePage(this, Program.form8);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("11"));
            thread.Start();
            Program.ChangePage(this, Program.form11);
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("2"));
            thread.Start();
            Program.ChangePage(this, Program.form2);
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            //Thread thread = new Thread(() => runAudio2("1"));
            //thread.Start();
            runAudio2("1");
            Program.HomeId = 46;
            Program.ChangePage(this, Program.formMoneyType);
        }

        private void FormOrganization_Load(object sender, EventArgs e)
        {
            GC.Collect();
            displayScreens();
            //this.runThread = new Thread(new ThreadStart(this.runAudio));
            //this.runThread.SetApartmentState(ApartmentState.STA);
            //this.runThread.Start();
        }

        public void runAudio2(String s)
        {
            playWave(s);
        }


        public void runAudio()
        {
            Thread thread = new Thread(() => playWave("selectDepartment"));
            thread.Start();
            //playWave("selectDepartment");
        }

        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath );
 
        }

        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formIdentity);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }
    }
}
