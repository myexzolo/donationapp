﻿namespace Donation.WinForm
{
    partial class FormBill2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.labRec = new System.Windows.Forms.Label();
            this.labRecBook = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelTotal = new System.Windows.Forms.Label();
            this.datePickerEnd = new System.Windows.Forms.Label();
            this.datePickerBegin = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::Donation.WinForm.Properties.Resources.Thai_government;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(101, -1);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(80, 80);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 23);
            this.label1.TabIndex = 8;
            this.label1.Text = "เล่มที่ ";
            this.label1.Visible = false;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("TH Sarabun New", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(-1, 79);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(280, 28);
            this.label5.TabIndex = 7;
            this.label5.Text = "รายงานสรุปผลจำนวนเงินที่ได้รับบริจาค";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(-1, 103);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(285, 28);
            this.label6.TabIndex = 6;
            this.label6.Text = "ในราชการกรมส่งเสริมและพัฒนาคุณภาพชีวิตคนพิการ";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(204, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 18);
            this.label2.TabIndex = 5;
            this.label2.Text = "เลขที่ ";
            this.label2.Visible = false;
            // 
            // labRec
            // 
            this.labRec.AutoSize = true;
            this.labRec.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRec.Location = new System.Drawing.Point(246, 2);
            this.labRec.Name = "labRec";
            this.labRec.Size = new System.Drawing.Size(33, 26);
            this.labRec.TabIndex = 4;
            this.labRec.Text = "005";
            this.labRec.Visible = false;
            // 
            // labRecBook
            // 
            this.labRecBook.AutoSize = true;
            this.labRecBook.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labRecBook.Location = new System.Drawing.Point(41, 2);
            this.labRecBook.Name = "labRecBook";
            this.labRecBook.Size = new System.Drawing.Size(33, 26);
            this.labRecBook.TabIndex = 3;
            this.labRecBook.Text = "162";
            this.labRecBook.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.labelTotal);
            this.panel1.Controls.Add(this.datePickerEnd);
            this.panel1.Controls.Add(this.datePickerBegin);
            this.panel1.Controls.Add(this.labRecBook);
            this.panel1.Controls.Add(this.labRec);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(283, 270);
            this.panel1.TabIndex = 7;
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelTotal.Location = new System.Drawing.Point(41, 219);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(179, 26);
            this.labelTotal.TabIndex = 15;
            this.labelTotal.Text = "รวมเป็นจำนวนเงิน XXXXX  บาท";
            // 
            // datePickerEnd
            // 
            this.datePickerEnd.AutoSize = true;
            this.datePickerEnd.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePickerEnd.Location = new System.Drawing.Point(12, 172);
            this.datePickerEnd.Name = "datePickerEnd";
            this.datePickerEnd.Size = new System.Drawing.Size(113, 26);
            this.datePickerEnd.TabIndex = 10;
            this.datePickerEnd.Text = "ถึง วันที่ xx/xx/xxxx";
            // 
            // datePickerBegin
            // 
            this.datePickerBegin.AutoSize = true;
            this.datePickerBegin.Font = new System.Drawing.Font("TH Sarabun New", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.datePickerBegin.Location = new System.Drawing.Point(12, 146);
            this.datePickerBegin.Name = "datePickerBegin";
            this.datePickerBegin.Size = new System.Drawing.Size(127, 26);
            this.datePickerBegin.TabIndex = 9;
            this.datePickerBegin.Text = "ตั้งแต่ วันที่ xx/xx/xxxx";
            // 
            // FormBill2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(283, 272);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormBill2";
            this.Text = "FormBill";
            this.Load += new System.EventHandler(this.FormBill_Load);
            this.Shown += new System.EventHandler(this.FormBill_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label labRec;
        private System.Windows.Forms.Label labRecBook;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label datePickerEnd;
        private System.Windows.Forms.Label datePickerBegin;
    }
}