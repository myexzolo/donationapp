﻿using System;
using System.Threading.Tasks;
using Donation.Models;
using Donation.WinForm.Models;

namespace Donation.Interface
{
    public interface IRest
    {
        Task<ResponseLoginByNationalId> LoginByNationalId(string nationalId,string clientName);
        Task<LoginResponse> GuestLogin(string clientName);
        Task<Response<Home>> GetHomeList();
        Task<Response<Province>> GetProvinceList();
        Task<Response<Amphur>> GetAmphurList(string provinceId);
        Task<Response<Tambol>> GetTambolList(string amphurId);
        Task<Response<VdoUrl>> GetPublicNewsList();
        Task<Response<MoneyObjective>> GetDonateMoneyObjective(string token);
        Task<Status> Register(RegisterParam param);
        Task<LoginResponse> Login(string email = "", string password = "");
        Task<Status> DonateMoney(string token, string money, string objective, string note,string homeId);
        Task<Status> DonateThings(string token, DonateThingsParam param,string homeId);
        Task<Response<Product>> ProductList(string token);
        Task<Response<History>> SearchDonateHistory(string token);
        Task<Response<HistoryItem>> SearchDonateItemHistory(string token, string donationId);
    }
}
