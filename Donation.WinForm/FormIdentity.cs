﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormIdentity : Form
    {
        private Thread runThread = null;
        public FormIdentity()
        {
            InitializeComponent();
            //
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this,Program.formVdo);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            try
            {
                Person person = ReadSmartCard.ReadData();
                Global.Person = null;
                if (person.Status.Equals("SUCCESS"))
                {
                    playWave("identity1");
                    Global.Person = person;
                    Task.Run(() => Login());
                    //Task.Run(() => { LoginByNationalId(person.pers); });
                }
                else
                {
                    //MessageBox.Show(person.Status);
                    /*Global.Person = new Person
                    {
                        Address = "x",
                        Amphure = "ยานนาวา",
                        IdCard = "1112223334445",
                        NameTh = "x",
                        SerNameTh = "x",
                        Moo = "x",
                        Soi = "x",
                        Road = "x",
                        Province = "กรุงเทพมหานคร",
                        Tumbol = "ช่องนนทรี",
                    };
                    Task.Run(() => Login());
                    */
                    playWave("insertCard");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }


        }
        async Task Login()
        {
            try
            {
                var Sername = Global.Person.SerNameTh.Trim();
                string amphur;
                string tambol;
                string province = Global.Person.Province.Trim().Replace("จังหวัด", "");
                if (province == "กรุงเทพมหานคร")
                {
                    amphur = Global.Person.Amphure.Replace("เขต", "");
                    tambol = Global.Person.Tumbol.Replace("แขวง", "");
                }
                else
                {
                    amphur = Global.Person.Amphure;
                    tambol = Global.Person.Tumbol;
                }
                amphur = amphur.Replace("อำเภอ", "");
                tambol = tambol.Replace("ตำบล", "");
                bool success = false;
                do
                {
                    var response = await Program.Rest.LoginByNationalId
                                                (Program.KioskName
                                                    , Global.Person.IdCard.Trim()
                                                    , Global.Person.NameTh.Trim()
                                                    , Sername
                                                    , Global.Person.Address.Trim()
                                                    , Global.Person.Moo.Trim()
                                                    , Global.Person.Soi.Trim()
                                                    , Global.Person.Road.Trim().Replace("ถนน", "")
                                                    , province
                                                    , amphur
                                                    , tambol);
                    if (response.status)
                    {
                        Program.Token = response.token;

                        this.Invoke(new MethodInvoker(() =>
                        {
                            Program.ChangePage(this, Program.formOrganization);
                        }));

                    }
                    else
                    {
                        // MessageBox.Show("กรุณา ต่ออินเทอร์เน็ต");
                    }
                    success = response.status;
                } while (!success);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }

        }
        
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            try
            {
                Task.Run(async () =>
                {
                    while (true)
                    {
                        var response = await Program.Rest.GuestLogin(Program.KioskName);
                        Program.Token = response.token;
                        if (response.status) break;

                    }
                    this.Invoke(new MethodInvoker(() =>
                    {
                        Global.Person = null;

                        playWave("identity2");
                        Program.ChangePage(this, Program.formOrganization);
                    }));
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private void FormIdentity_Load(object sender, EventArgs e)
        {
            displayScreens();

        }

        public void runAudio()
        {
            //this.runThread = new Thread(new ThreadStart(this.runAudio));
            //this.runThread.SetApartmentState(ApartmentState.STA);
            //this.runThread.Start();
            Thread thread = new Thread(() => playWave("selectType"));
            thread.Start();
           
        }

        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath);

        }

        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
