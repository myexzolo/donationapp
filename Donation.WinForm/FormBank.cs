﻿using Donation.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormBank : Form
    {
        int amount = 0;
        public FormBank()
        {
            InitializeComponent();
            string format = "dd/MM/yyyy HH:mm";
            DateTime now = DateTime.Now;
            dateTxt.Text = "วันที่ " + now.ToString(format);

        }
        public void SetParam(BankAccount bankAccount)
        {
            labName.Text = "   กรุณาโอนเงิน ผ่านบัญชี ธ."+bankAccount.BankName
                + " สาขา" + bankAccount.Branch
                + "\r\nประเภทบัญชี "+ bankAccount.AccountType
                +" เลขที่บัญชี "+bankAccount.AccountNumber;

        }
        public static string ThaiBaht(string txt)
        {
            string bahtTxt, n, bahtTH = "";
            double amount;
            try { amount = Convert.ToDouble(txt); }
            catch { amount = 0; }
            bahtTxt = amount.ToString("####.00");
            string[] num = { "ศูนย์", "หนึ่ง", "สอง", "สาม", "สี่", "ห้า", "หก", "เจ็ด", "แปด", "เก้า", "สิบ" };
            string[] rank = { "", "สิบ", "ร้อย", "พัน", "หมื่น", "แสน", "ล้าน" };
            string[] temp = bahtTxt.Split('.');
            string intVal = temp[0];
            string decVal = temp[1];
            if (Convert.ToDouble(bahtTxt) == 0)
                bahtTH = "ศูนย์บาทถ้วน";
            else
            {
                for (int i = 0; i < intVal.Length; i++)
                {
                    n = intVal.Substring(i, 1);
                    if (n != "0")
                    {
                        if ((i == (intVal.Length - 1)) && (n == "1"))
                            bahtTH += "เอ็ด";
                        else if ((i == (intVal.Length - 2)) && (n == "2"))
                            bahtTH += "ยี่";
                        else if ((i == (intVal.Length - 2)) && (n == "1"))
                            bahtTH += "";
                        else
                            bahtTH += num[Convert.ToInt32(n)];
                        bahtTH += rank[(intVal.Length - i) - 1];
                    }
                }
                bahtTH += "บาท";
                if (decVal == "00")
                    bahtTH += "ถ้วน";
                else
                {
                    for (int i = 0; i < decVal.Length; i++)
                    {
                        n = decVal.Substring(i, 1);
                        if (n != "0")
                        {
                            if ((i == decVal.Length - 1) && (n == "1"))
                                bahtTH += "เอ็ด";
                            else if ((i == (decVal.Length - 2)) && (n == "2"))
                                bahtTH += "ยี่";
                            else if ((i == (decVal.Length - 2)) && (n == "1"))
                                bahtTH += "";
                            else
                                bahtTH += num[Convert.ToInt32(n)];
                            bahtTH += rank[(decVal.Length - i) - 1];
                        }
                    }
                    bahtTH += "สตางค์";
                }
            }
            return bahtTH;
        }

        private void FormBill_Load(object sender, EventArgs e)
        {
            this.Focus();
          
            PrintDocument print = new PrintDocument();
            print.PrintPage += new PrintPageEventHandler(PrintImage);
            print.Print();
            this.Close();
        }
        void PrintImage(object o, PrintPageEventArgs e)
        {
            int width = this.Width;
            int height = this.Height;

            Graphics g1 = this.CreateGraphics();

            Rectangle bounds = new Rectangle(0, 0, width, height);
            Bitmap img = new Bitmap(width, height, g1);

            this.DrawToBitmap(img, bounds);
            e.Graphics.CompositingQuality = CompositingQuality.HighQuality;
            e.Graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
            e.Graphics.SmoothingMode = SmoothingMode.HighQuality;
            e.Graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
            e.Graphics.DrawImage(img, bounds, 0, 0, img.Width, img.Height, System.Drawing.GraphicsUnit.Pixel);
        }

        private void FormBill_Shown(object sender, EventArgs e)
        {
            

        }
    }
}
