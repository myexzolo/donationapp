﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    public interface IRestPlatform7Front
    {
        [Post("/pifront")]
        Task PiFront([Refit.Body(BodySerializationMethod.Json)]PiFrontParam param);
    }
}
