﻿using System;
namespace Donation.Models
{
    public class HistoryItem
    {
        public string item { get; set; }
        public string quantity { get; set; }
        public int countingNumber { get; set; }
        public string unitMeasure { get; set; }
        public string price { get; set; }

        public string Item => item ?? string.Empty;
        public string QuantityPerUnit => quantity ?? string.Empty;
        public int Count => countingNumber;
        public string Unit => unitMeasure ?? "ชิ้น";
        public string PricePerUnit => price ?? string.Empty;
    }
}
