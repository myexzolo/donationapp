﻿using System;
namespace Donation.Models
{
	public class UserJson
	{
		public UserJson()
		{
		}

		public string nationalid { get; set; }
		public bool isorg { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string title { get; set; }
		public int age { get; set; }
		public string birthdate { get; set; }
		public string addressname { get; set; }
		public int floor { get; set; }
		public string addressnumber { get; set; }
		public string moo { get; set; }
		public string soi { get; set; }
		public string street { get; set; }
		public string province { get; set; }
		public string amphur { get; set; }
		public string tumbol { get; set; }
		public string zipcode { get; set; }
		public string telephone { get; set; }
		public string email { get; set; }
	}
}
