﻿using System;
namespace Donation.Models
{
    public class MoneyObjective
    {
        public int id { get; set; }
        public string name { get; set; }

        public int Id => id;
        public string Name => name ?? string.Empty;
    }
}
