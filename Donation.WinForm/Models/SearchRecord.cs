﻿using System;
using System.Collections.Generic;

namespace Donation.Models
{
    public class SearchRecord
    {
        public string id { get; set; }
        public string date_string { get; set; }
        public string DateString => date_string ?? string.Empty;
        public bool is_money { get; set; }
        public string Is
        {
            get
            {
                if (is_money)
                {
                    return "เป็นจำนวนเงิน";
                }
                else
                {
                    return "เป็นสิ่งของ";
                }
            }
        }
        public decimal money { get; set; }
        public string Money
        {
            get
            {
                if (is_money)
                {
                    return money.ToString() + " บาท";
                }
                else
                {
                    return string.Empty;
                }
            }
        }
        public string organization { get; set; }
        public string Organization => organization ?? string.Empty;

        static public Random Random { get; set; } = new Random();
        static public SearchRecord RandomSearchRecord()
        {
            return new SearchRecord
            {
                id = Random.Next().ToString(),
                date_string = DateTime.Now.AddDays(-Random.Next(365)).ToShortDateString(),
                is_money = Random.Next() % 2 == 0,
                money = 100 * (Random.Next() % 100),
                organization = "กทม."
            };
        }
        static public List<SearchRecord> DefaultList()
        {
            var list = new List<SearchRecord>();
            for (int i = 0; i < 10;i++)
            {
                list.Add(RandomSearchRecord());
            }
            return list;
        }
    }
}
