﻿using System;

namespace Donation.Models
{
    public class Status 
    {
        public bool status { get; set; }
        public string code { get; set; }
        public string message { get; set; }
    }
}

