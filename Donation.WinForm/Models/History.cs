﻿using System;
namespace Donation.Models
{
    public class History
    {
        public string donateId { get; set; }
        public string donateDate { get; set; }
        public bool donateMoney { get; set; }
        public string amountMoney { get; set; }
        public bool donateItem { get; set; }
        public string organization { get; set; }
        public string Organization => organization ?? string.Empty;

        public string Money => (amountMoney ?? string.Empty) + " บาท";
        public string DateString => donateDate ?? string.Empty;
        public string Is
        {
            get
            {
                if (donateMoney)
                {
                    if(donateItem)
                    {
                        return "เป็นสิ่งของ และเป็นจำนวนเงิน";
                    }
                    else
                    {
                        return "เป็นจำนวนเงิน";
                    }
                }
                else
                {
                    return "เป็นสิ่งของ";
                }
            }
        }

    }
}
