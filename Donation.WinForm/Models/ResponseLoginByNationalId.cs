﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm.Models
{
    public class ResponseLoginByNationalId
    {
        public bool status { get; set; }
        public string token { get; set; }
        public int donatorId { get; set; }
        public string donatorName { get; set; }
    }
    //{
}
