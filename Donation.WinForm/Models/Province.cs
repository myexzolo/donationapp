﻿using System;
namespace Donation.Models
{
    public class Province
    {
        public string provinceId { get; set; }
        public string provinceName { get; set; }

        public string Id => provinceId ?? string.Empty;
        public string Name => provinceName ?? string.Empty;
    }
}
