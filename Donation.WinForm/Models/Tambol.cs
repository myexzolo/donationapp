﻿using System;
namespace Donation.Models
{
    public class Tambol
    {
        public string tambolId { get; set; }
        public string tambolName { get; set; }

        public string Id => tambolId ?? string.Empty;
        public string Name => tambolName ?? string.Empty;
    }
}
