﻿using System;
using System.Collections.Generic;

namespace Donation.Models
{
    public class DonateThingsParam
    {
        public List<Thing> items { get; set; }
    }
}
