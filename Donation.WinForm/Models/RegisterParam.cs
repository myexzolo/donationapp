﻿using System;
namespace Donation.Models
{
    public class RegisterParam
    {
        public string donatorType { get; set; }
        public string psnId { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string birthDate { get; set; } // :- 11/09/2561
        public int age { get; set; }
        public string village { get; set; }
        public string floor { get; set; }
        public string homeNo { get; set; }
        public string mooNo { get; set; }
        public string soi { get; set; }
        public string provinceId { get; set; }
        public string amphurId { get; set; }
        public string tambolId { get; set; }
        public string postalCode { get; set; }
        public string tel { get; set; }
        public string email { get; set; }
        public string password { get; set; }
    }
}
