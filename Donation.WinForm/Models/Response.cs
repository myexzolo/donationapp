﻿using System;
using System.Collections.Generic;

namespace Donation.Models
{
    public class Response<T>
    {
        public bool status { get; set; }
        public List<T> data { get; set; }
    }
    public class LoginResponse
    {
        public bool status { get; set; }
        public string token { get; set; }
    }
    public class ForReport
    {
        public bool status { get; set; }
        public string moneyReceiptBookNumber { get; set; }
        public string moneyReceiptNumber { get; set; }
        public string departmentUnitName { get; set; }
        public string donateMoneyNote { get; set; }
    }
}
