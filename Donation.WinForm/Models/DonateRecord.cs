﻿using System;
using System.Collections.Generic;

namespace Donation.WinForm.Models
{
    public class DonateRecord
    {
        public string donateDate { get; set; }
        public string amountMoney { get; set; }
    }
    public class ResponseForPrint
    {
        public bool status { get; set; }
        public List<DonateRecord> data { get; set; }

        public string total { get; set; }
    }
}
