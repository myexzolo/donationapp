﻿using System;
namespace Donation.WinForm.Models
{
    public class BankAccount
    {
        public string accountName { get; set; }
        public string accountNumber { get; set; }
        public string accountType { get; set; }
        public string bankName { get; set; }
        public string branch { get; set; }

        public string AccountNumber => (accountNumber ?? string.Empty);
        public string AccountType => (accountType ?? string.Empty);
        public string BankName => ((bankName == null) ? string.Empty : bankName.Replace("ธนาคาร", ""));
        public string Branch => ((branch == null) ? string.Empty : branch.Replace("สาขา", ""));
    }
}
