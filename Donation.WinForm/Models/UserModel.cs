﻿using System;
using System.Text.RegularExpressions;

namespace Donation.Models
{
	public class UserModel
	{
		public UserModel(UserJson json)
		{
			if (json == null) throw new ArgumentNullException();

			IsOrg = json.isorg;
			if (string.IsNullOrWhiteSpace(json.firstname)) throw new ArgumentException("กรุณากรอกชื่อให้ถูกต้อง");
			if (json.firstname.Length > 200) throw new ArgumentException("ไม่อนุญาตให้กรอกชื่อยาวเกิน 200 ตัวอักษร");
			FirstName = json.firstname;
			if (json.lastname == null) throw new ArgumentException("นามสกุลต้องไม่เป็น null");
			if (json.lastname.Length > 200) throw new ArgumentException("ไม่อนุญาตให้กรอกนามสกุลยาวเกิน 200 ตัวอักษร");
			LastName = json.lastname;
			if (string.IsNullOrEmpty(json.title)) throw new ArgumentException("กรุณากรอกคำนำหน้าให้ถูกต้อง");
			if (json.title.Length > 100) throw new ArgumentException("ไม่อนุญาตให้กรอกคำนำหน้ายาวเกิน 100 ตัวอักษร");
			Title = json.title;
			if (json.age <= 0) throw new ArgumentException("อายุต้องมากกว่า 0");
			if (json.age > 200) throw new ArgumentException("อายุต้องน้อยกว่า 200");
			Age = json.age;
			try
			{
				BirthDate = DateTime.Parse(json.birthdate);
			}
			catch (FormatException)
			{
				throw new ArgumentException("วันเกิดไม่อยู่ในรูปแบบที่ถูกต้อง");
			}

			if (json.addressname == null) throw new ArgumentException("ที่อยู่ต้องไม่เป็น null");
			if (json.addressname.Length > 400) throw new ArgumentException("ไม่อนุญาตให้กรอกที่อยู่ยาวเกิน 200 ตัวอักษร");
			AddressName = json.addressname;

			if (json.floor < 0) throw new ArgumentException("ชั้นที่ ต้องไม่น้อยกว่าศูนย์");
			if (json.floor > 1000) throw new ArgumentException("ชั้นที่ ต้องน้อยกว่าพัน");
			Floor = json.floor;

			if (json.addressnumber == null) throw new ArgumentException("เลขที่บ้านต้องไม่เป็น null");
			if (json.addressnumber.Length > 200) throw new ArgumentException("ไม่อนุญาตให้เลขที่บ้านยาวเกิน 200 ตัวอักษร");
			AddressNumber = json.addressnumber;

			if (json.moo == null) throw new ArgumentException("หมู่ต้องไม่เป็น null");
			if (json.moo.Length > 200) throw new ArgumentException("ไม่อนุญาตให้หมู่ยาวเกิน 200 ตัวอักษร");
			Moo = json.moo;

			if (json.soi == null) throw new ArgumentException("ซอยต้องไม่เป็น null");
			if (json.soi.Length > 200) throw new ArgumentException("ไม่อนุญาตให้ซอยยาวเกิน 200 ตัวอักษร");
			Soi = json.soi;

			if (json.street == null) throw new ArgumentException("ถนนต้องไม่เป็น null");
			if (json.street.Length > 200) throw new ArgumentException("ไม่อนุญาตให้ถนนยาวเกิน 200 ตัวอักษร");
			Street = json.street;

			if (json.province == null) throw new ArgumentException("จังหวัดต้องไม่เป็น null");
			if (json.province.Length > 200) throw new ArgumentException("ไม่อนุญาตให้จังหวัดยาวเกิน 200 ตัวอักษร");
			Province = json.province;

			if (json.amphur == null) throw new ArgumentException("อำเภอต้องไม่เป็น null");
			if (json.amphur.Length > 200) throw new ArgumentException("ไม่อนุญาตให้เลขที่บ้านยาวเกิน 200 ตัวอักษร");
			Amphur = json.amphur;
            
            
			if (json.zipcode == null) throw new ArgumentException("รหัสไปรษณีย์ต้องไม่เป็น null");
			var rx = new Regex(@"^\d\d\d\d\d$");
			if(!rx.IsMatch(json.zipcode))
			{
				throw new ArgumentException("รหัสไปรษณีย์ต้องเป็นตัวเลข 5 ตัวเท่านั้น");
			}
			ZipCode = json.zipcode;
            
			if (json.telephone == null) throw new ArgumentException("เบอร์โทรต้องไม่เป็น null");
            if (json.telephone.Length > 200) throw new ArgumentException("ไม่อนุญาตให้เบอร์โทรยาวเกิน 200 ตัวอักษร");
            Telephone = json.telephone;

			if (json.email == null) throw new ArgumentException("อีเมลต้องไม่เป็น null");
			Regex regex = new Regex(@"^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$");
			if (!rx.IsMatch(json.email)) throw new ArgumentException("กรุณากรอกอีเมล์ที่อยู่ในรูปแบบที่ถูกต้อง เช่น test@gmail.com");
			Email = json.email;

		}
			
		public string NationalId { get; set; }
		public bool IsOrg { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Title { get; set; }
		public int Age { get; set; }
		public DateTime BirthDate { get; set; }
		public string AddressName { get; set; }
		public int Floor { get; set; }
		public string AddressNumber { get; set; }
		public string Moo { get; set; }
		public string Soi { get; set; }
		public string Street { get; set; }
		public string Province { get; set; }
		public string Amphur { get; set; }
		public string Tumbol { get; set; }
		public string ZipCode { get; set; }
		public string Telephone { get; set; }
		public string Email { get; set; }
	}
}


