﻿using System;
namespace Donation.Models
{
    public class Amphur
    {
        public string amphurId { get; set; }
        public string amphurName { get; set; }

        public string Id => amphurId ?? string.Empty;
        public string Name => amphurName ?? string.Empty;
    }
}
