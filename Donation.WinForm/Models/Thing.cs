﻿namespace Donation.Models
{
    public class Thing
    {
        public string id { get; set; }
        public string name { get; set; }
        public double amount { get; set; }
        public string unitName { get; set; }
        public string priceAll { get; set; }
        public string unitId { get; set; }

    }
}
