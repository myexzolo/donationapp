﻿using System;
namespace Donation.Models
{
    public class Product
    {
        public string itemId { get; set; }
        public string itemName { get; set; }
        public string quantity { get; set; }
        public string unitName { get; set; }
        public string unitId { get; set; }

        public string Id => itemId ?? new Random().Next(1,100).ToString();
        public string Name => itemName ?? "สมมติ";
        public string Unit => unitName ?? "ชิ้น";
        public string UnitId => unitId ?? string.Empty;
    }
}
