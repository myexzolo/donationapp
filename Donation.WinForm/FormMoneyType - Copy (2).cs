﻿using Donation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormLetter : Form
    {
        public FormLetter()
        {
            InitializeComponent();
        }

        public List<MoneyObjective> TopicList { get; set; }

        private void FormMoneyType_Load(object sender, EventArgs e)
        {
            displayScreens();
            while (Program.Token == null)
            {
                Thread.Sleep(50);
            }
            Task.Run(() => RefreshList());
            Global.ReceiptLetter = false;
            Global.ThankYouLetter = false;
            ReflexLetter();
            
        }


        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }

        async Task RefreshList()
        {
            var response = await Program.Rest.GetDonateMoneyObjective(Program.Token);
            if (response.status)
            {
                TopicList = response.data;
                this.Invoke(new MethodInvoker(() =>
                {
                    var NameList = TopicList.Select(x => x.Name);
                    foreach (var each in NameList)
                    {
                        //listBox1.Items.Add(each);
                    }
                }));
                //Device.BeginInvokeOnMainThread(() => RaisePropertyChanged(nameof(TopicList)));
            }
            else
            {
                //PleaseConnectInternet();
            }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formDonate);
        }

        private void FormMoneyType_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            Program.ChangePage(this, Program.formOrganization);
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
        void ReflexLetter()
        {
            pictureBoxReceipt.Visible = Global.ReceiptLetter;
            pictureBoxThankYou.Visible = Global.ThankYouLetter;

        }
        private void pictureBox3_Click(object sender, EventArgs e)
        { //2 ทั่วไป
            Global.ReceiptLetter = !Global.ReceiptLetter;
            ReflexLetter();
            //Program.ChangePage(this, Program.formDonate);
            //Program.ChangePage(this, Program.formMoneyType2);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ///Program.ChangePage(this, Program.formMoneyType2);
            Global.ThankYouLetter = !Global.ThankYouLetter;
            ReflexLetter();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formMoneyType2);
        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
