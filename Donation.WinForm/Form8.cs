﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class Form8 : Form
    {
        public Form8()
        {
            InitializeComponent();
        }

        private void Donate()
        {
            Program.ChangePage(this,Program.formMoneyType);
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("phapadang"));
            thread.Start();
            Program.HomeId = 10;
            Donate();
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NakhonSiThammarat"));
            thread.Start();
            Program.HomeId = 40;
            Donate();
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            
            Thread thread = new Thread(() => runAudio2("KhonKaen"));
            thread.Start();
            Program.HomeId = 42;
            Donate();
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            
            Thread thread = new Thread(() => runAudio2("NongKhai"));
            thread.Start();
            Program.HomeId = 44;
            Donate();
        }

        private void pictureBox5_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("LopBuri"));
            thread.Start();
            Program.HomeId = 48;
            Donate();
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("RainChiangMai"));
            thread.Start();
            Program.HomeId = 34;
            Donate();
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("TongUbonRatchathani"));
            thread.Start();
            Program.HomeId = 38;
            Donate();
        }

        private void pictureBox8_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("sriUbonRatchathani"));
            thread.Start();
            Program.HomeId = 12;
            Donate();
        }

        private void Form8_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            Program.ChangePage(this, Program.formOrganization);
        }

        private void pictureBox9_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formOrganization);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formOrganization);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        public void runAudio2(String s)
        {
            playWave(s);
        }


        public void runAudio()
        {
            playWave("selectDepartment");
        }

        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath);

        }

        private void Form8_Load(object sender, EventArgs e)
        {
            displayScreens();
        }
        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }
    }
}
