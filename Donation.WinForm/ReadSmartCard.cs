﻿using RDNIDWRAPPER;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    public class ReadSmartCard
    {
        public static Person ReadData()
        {
            Person person = new Person();

            RDNIDWRAPPER.RDNID mRDNIDWRAPPER = new RDNIDWRAPPER.RDNID();
            string StartupPath = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetEntryAssembly().Location);

            string fileName = StartupPath + "\\RDNIDLib.DLX";
            if (System.IO.File.Exists(fileName) == false)
            {
                person.Status = "RDNIDLib.DLX not found";
                return person;
                //MessageBox.Show("RDNIDLib.DLX not found");
            }

            byte[] _lic = String2Byte(fileName);


            int nres = 0;
            nres = RDNID.openNIDLibRD(_lic);
            if (nres != 0)
            {
                String m;
                m = String.Format(" error no {0} ", nres);
                person.Status = ErrCode(nres.ToString());
                return person;
                //MessageBox.Show(m);
            }

            byte[] Licinfo = new byte[1024];

            RDNID.getLicenseInfoRD(Licinfo);

            string strTerminal = ListCardReader();

            IntPtr obj = selectReader(strTerminal);



            Int32 nInsertCard = 0;
            nInsertCard = RDNID.connectCardRD(obj);
            if (nInsertCard != 0)
            {
                String m;
                m = String.Format(" error no {0} ", nInsertCard);

                person.Status = ErrCode(nInsertCard.ToString());

                RDNID.disconnectCardRD(obj);
                RDNID.deselectReaderRD(obj);

                //person.IdCard = "1580400006441";
                //person.TitleTh = "นาย";
                //person.TitleEn = "Mr.";
                //person.NameTh = "สุทธิพันธ์";
                //person.NameEn = "Suthipan";
                //person.MidNameTh = "";
                //person.MidNameEn = "";
                //person.SerNameTh = "ป้องสุวรรณ";
                //person.SerNameEn = "Pongsuwan";
                //person.BirthDate = "";

                //person.Address = "402";
                //person.Moo = "หมู่ที่ 12";
                //person.Trok = "";
                //person.Soi = "";
                //person.Road = "";
                //person.Tumbol = "ตำบลบ้านกาศ";
                //person.Amphure = "อำเภอแม่สะเรียง";
                //person.Province = "จังหวัดแม่ฮ่องสอน";
                //person.Gender = "1";

                //person.IssueDate = "25600818";
                //person.ExpiryDate = "25690524";

                return person;
            }

            byte[] id = new byte[30];
            int res = RDNID.getNIDNumberRD(obj, id);
            if (res != DefineConstants.NID_SUCCESS)
            {
                person.Status = ErrCode(res.ToString());
                return person;
            }

            String NIDNum = aByteToString(id);



            byte[] data = new byte[1024];
            res = RDNID.getNIDTextRD(obj, data, data.Length);
            if (res != DefineConstants.NID_SUCCESS)
            {
                person.Status = ErrCode(res.ToString());
                return person;
            }


            String NIDData = aByteToString(data);
            if (NIDData == "")
            {
                person.Status = "Read Text error";
            }
            else
            {
                string[] fields = NIDData.Split('#');

                person.IdCard = NIDNum;
                person.TitleTh = fields[(int)NID_FIELD.TITLE_T];
                person.TitleEn = fields[(int)NID_FIELD.TITLE_E];
                person.NameTh = fields[(int)NID_FIELD.NAME_T];
                person.NameEn = fields[(int)NID_FIELD.NAME_E];
                person.MidNameTh = fields[(int)NID_FIELD.MIDNAME_T];
                person.MidNameEn = fields[(int)NID_FIELD.MIDNAME_E];
                person.SerNameTh = fields[(int)NID_FIELD.SURNAME_T];
                person.SerNameEn = fields[(int)NID_FIELD.SURNAME_E];
                person.BirthDate = fields[(int)NID_FIELD.BIRTH_DATE];

                person.Address = fields[(int)NID_FIELD.HOME_NO];
                person.Moo = fields[(int)NID_FIELD.MOO];
                person.Trok = fields[(int)NID_FIELD.TROK];
                person.Soi = fields[(int)NID_FIELD.SOI];
                person.Road = fields[(int)NID_FIELD.ROAD];
                person.Tumbol = fields[(int)NID_FIELD.TUMBON];
                person.Amphure = fields[(int)NID_FIELD.AMPHOE];
                person.Province = fields[(int)NID_FIELD.PROVINCE];
                person.Gender = fields[(int)NID_FIELD.GENDER];

                person.IssueDate = fields[(int)NID_FIELD.ISSUE_DATE];
                person.ExpiryDate = fields[(int)NID_FIELD.EXPIRY_DATE];
                person.Status = "SUCCESS";
            }

            //byte[] NIDPicture = new byte[1024 * 5];
            //int imgsize = NIDPicture.Length;
            //res = RDNID.getNIDPhotoRD(obj, NIDPicture, out imgsize);
            //if (res != DefineConstants.NID_SUCCESS)
            //{
            //    person.Status = ErrCode(res.ToString());
            //    return person;
            //}


            //byte[] byteImage = NIDPicture;
            //if (byteImage == null)
            //{
            //    MessageBox.Show("Read Photo error");
            //}
            //else
            //{
            //    //m_picPhoto
            //    Image img = Image.FromStream(new MemoryStream(byteImage));
            //    Bitmap MyImage = new Bitmap(img, m_picPhoto.Width - 2, m_picPhoto.Height - 2);
            //    m_picPhoto.Image = (Image)MyImage;
            //}

            RDNID.disconnectCardRD(obj);
            RDNID.deselectReaderRD(obj);

            return person;
        }

        private static string ErrCode(string res)
        {
            string strErr = "";
            if (res == "-1")
            {
                strErr = "เกิดข้อผิดพลาดในระบบหรือไม่พบบัตรประชาชน";
            }
            else if (res == "-2")
            {
                strErr = "ใบอนุญาติ";
            }
            else if (res == "-3")
            {
                strErr = "ไม่พบเครื่องอ่านบัตร";
            }
            else if (res == "-4")
            {
                strErr = "ไม่สามารถเชื่อมต่อกับเครื่องอ่าน";
            }
            else if (res == "-5")
            {
                strErr = "ไม่สามารถอ่านรูปได้";
            }
            else if (res == "-6")
            {
                strErr = "ไม่สามารถอ่านข้อมูลตัวอักษรหน้าบัตรประชาชน";
            }
            else if (res == "-7")
            {
                strErr = "บัตรที่อ่านไม่ใช่บัตรประชาชน";
            }
            else if (res == "-8")
            {
                strErr = "ไม่รองรับการใช้งานกับบัตรรุ่นนี้";
            }
            else if (res == "-9")
            {
                strErr = "ไม่สามารถยกเลิกการเชื่อมต่อกับเครื่องอ่านบัตรได้";
            }
            else if (res == "-10")
            {
                strErr = "ยังไม่ได้เรียกใช้งานฟังก์ชั่น OpenLibNIDRD เพื่อเริ่มต้นการทำงานให้กับ Library";
            }
            else if (res == "-11")
            {
                strErr = "ไม่รองรับการใช้งานกับเครื่องอ่านนี้ หรือไม่พบเครื่องอ่านบัตร";
            }
            else if (res == "-12")
            {
                strErr = "ไม่พบแฟ้มใบอนุญาติหรือแฟ้มใบอนุญาติเสียหาย";
            }
            else if (res == "-13")
            {
                strErr = "Parameter ผิดพลาด";
            }
            else if (res == "-14")
            {
                strErr = "ไม่สามารถติดต่อ Internet ได้";
            }
            else if (res == "-15")
            {
                strErr = "ไม่พบบัตร";
            }
            else if (res == "-16")
            {
                strErr = "อัปเดตแฟ้มใบอนุญาติไม่สำเร็จ";
            }
            return strErr;
        }
        private static String ListCardReader()
        {
            String str = "";
            byte[] szReaders = new byte[1024 * 2];
            int size = szReaders.Length;
            int numreader = RDNID.getReaderListRD(szReaders, size);
            if (numreader <= 0)
                str = "";
            String s = aByteToString(szReaders);
            String[] readlist = s.Split(';');
            if (readlist != null)
            {
                str = readlist[0];
            }
            return str;
        }

        public static IntPtr selectReader(String reader)
        {
            IntPtr mCard = (IntPtr)0;
            byte[] _reader = String2Byte(reader);
            IntPtr res = (IntPtr)RDNID.selectReaderRD(_reader);
            if ((Int64)res > 0)
                mCard = (IntPtr)res;
            return mCard;
        }

        static string aByteToString(byte[] b)
        {
            Encoding ut = Encoding.GetEncoding(874); // 874 for Thai langauge
            int i;
            for (i = 0; b[i] != 0; i++) ;

            string s = ut.GetString(b);
            s = s.Substring(0, i);
            return s;
        }

        enum NID_FIELD
        {
            NID_Number,   //1234567890123#

            TITLE_T,    //Thai title#
            NAME_T,     //Thai name#
            MIDNAME_T,  //Thai mid name#
            SURNAME_T,  //Thai surname#

            TITLE_E,    //Eng title#
            NAME_E,     //Eng name#
            MIDNAME_E,  //Eng mid name#
            SURNAME_E,  //Eng surname#

            HOME_NO,    //12/34#
            MOO,        //10#
            TROK,       //ตรอกxxx#
            SOI,        //ซอยxxx#
            ROAD,       //ถนนxxx#
            TUMBON,     //ตำบลxxx#
            AMPHOE,     //อำเภอxxx#
            PROVINCE,   //จังหวัดxxx#

            GENDER,     //1#			//1=male,2=female

            BIRTH_DATE, //25200131#	    //YYYYMMDD 
            ISSUE_PLACE,//xxxxxxx#      //
            ISSUE_DATE, //25580131#     //YYYYMMDD 
            EXPIRY_DATE,//25680130      //YYYYMMDD 
            ISSUE_NUM,  //12345678901234 //14-Char
            END
        };

        static byte[] String2Byte(string s)
        {
            // Create two different encodings.
            //Encoding ascii = Encoding.GetEncoding(874);
            Encoding ascii = Encoding.ASCII;
            Encoding unicode = Encoding.Unicode;

            // Convert the string into a byte array.
            byte[] unicodeBytes = unicode.GetBytes(s);

            // Perform the conversion from one encoding to the other.
            byte[] asciiBytes = Encoding.Convert(unicode, ascii, unicodeBytes);

            return asciiBytes;
        }

        public static string GetCurrentExecutingDirectory(System.Reflection.Assembly assembly)
        {
            string filePath = new Uri(System.Reflection.Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            return Path.GetDirectoryName(filePath);
        }

    }
}
