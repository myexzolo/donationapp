﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormSearchItem : Form
    {
        public FormSearchItem()
        {
            InitializeComponent();
        }

        private void FormSearchItem_Load(object sender, EventArgs e)
        {
            foreach(var each in Program.HistoryItemList)
            {
                var item = new ListViewItem(each.Item);
                item.SubItems.Add(each.Count.ToString());
                item.SubItems.Add(each.PricePerUnit);
                item.SubItems.Add(each.QuantityPerUnit);
                listView1.Items.Add(item);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formSearch);
        }
    }
}
