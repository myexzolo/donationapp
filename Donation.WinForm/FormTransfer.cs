﻿using Donation.Shared;
using Donation.WinForm.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormTransfer : Form
    {
        public FormTransfer()
        {
            InitializeComponent();
        }


        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            displayScreens();
        }

        private void FormVdo_Click(object sender, EventArgs e)
        {
            NextForm();
        }

        public void runAudio2(String s)
        {
            playWave(s);
        }


        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath);

        }

        void NextForm()
        {
            //Program.ChangePage(this, Program.formIdentity);
            Program.ChangePage(this, Program.formIdentity);
        }

        private void label1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formIdentity);
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formMoneyType2);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            FormBank formBank = new FormBank();
            Program.Rest.GetBankAccountFromHomeId(Program.HomeId
                ).ContinueWith((arg) => 
            {
                formBank.SetParam(arg.Result.data.FirstOrDefault() ?? new Models.BankAccount());
                this.Invoke(new MethodInvoker(() =>
                {
                    
                    Thread thread = new Thread(() => runAudio2("slipTrasfer"));
                    thread.Start();
                    formBank.ShowDialog();
                    Program.ChangePage(this, Program.formVdo);
                }));
            });
            
        }

        void SetParam(BankAccount bankAccount)
        {
            if (bankAccount == null) return;

            labelBankname.Text = "ธนาคาร "+bankAccount.BankName ?? string.Empty;
            labelBranch.Text = "สาขา "+bankAccount.Branch ?? string.Empty;
            labelBranch.Text = "ปรเภทบัญชี "+bankAccount.AccountType ?? string.Empty;
            labelAccountNumber.Text = "เลขบัญชี "+bankAccount.AccountNumber ?? string.Empty;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }


        private void FormTransfer_Shown_1(object sender, EventArgs e)
        {
            Program.Rest.GetBankAccountFromHomeId(Program.HomeId
            ).ContinueWith((arg) =>
            {
                this.Invoke(new MethodInvoker(() =>
                {
                    this.SetParam(arg.Result.data.FirstOrDefault() ?? new Models.BankAccount());

                }));
            });
        }
    }
}
