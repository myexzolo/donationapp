﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormHome : Form
    {
        public FormHome()
        {
            InitializeComponent();
        }
         
        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formSearch);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formOrganization);
        }

        private void FormHome_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;

            Program.ChangePage(this, Program.formMoneyType);

        }
    }
}
