﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FrmLoading : Form
    {
        public FrmLoading()
        {
            InitializeComponent();
        }

        private void panel4_Paint(object sender, PaintEventArgs e)
        {
            displayScreens();
        }

        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }
    }
}
