﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Donation.WinForm
{
    public class Global
    {
        public static string ComPort = "";
        public static byte Validator1SSPAddress = 0;
        public static byte Validator2SSPAddress = 0;
        public static bool Comms = false;

        public static int coincount = 0;
        public static int coinbaht = 0;
        public static int total = 0;
        public static int bankcount = 0;
        public static int bankbaht = 0;
        public static int bankRecycle = 0;

        public static int coin1 = 0;
        public static int coin2 = 0;
        public static int coin5 = 0;
        public static int coin10 = 0;

        public static bool changeCoin = false;

        public static int Allcoinbaht;
        public static int Allbankbaht;
        public static int AllTotal;

        public static Person Person { get; set; }

        public static bool ReceiptLetter;
        public static bool ThankYouLetter;
    }
}
