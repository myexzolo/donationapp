﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    public class Person
    {
        private string _idCard;
        private string _titleTh;
        private string _nameTh;
        private string _midNameTh;
        private string _serNameTh;
        private string _titleEn;
        private string _nameEn;
        private string _midNameEn;
        private string _serNameEn;
        private string _gender;
        private string _birthDate;
        private string _address;
        private string _moo;
        private string _trok;
        private string _soi;
        private string _road;
        private string _tumbol;
        private string _amphure;
        private string _province;
        private string _issueDate;
        private string _expiryDate;
        private string _status;


        public string Status
        {
            set { _status = value; }
            get { return _status; }
        }

        public string IssueDate
        {
            set { _issueDate = value; }
            get { return _issueDate; }
        }

        public string ExpiryDate
        {
            set { _expiryDate = value; }
            get { return _expiryDate; }
        }

        public string MidNameTh
        {
            set { _midNameTh = value; }
            get { return _midNameTh; }
        }

        public string MidNameEn
        {
            set { _midNameEn = value; }
            get { return _midNameEn; }
        }
        public string IdCard
        {
            set { _idCard = value; }
            get { return _idCard; }
        }
        public string TitleTh
        {
            set { _titleTh = value; }
            get { return _titleTh; }
        }
        public string NameTh
        {
            set { _nameTh = value; }
            get { return _nameTh; }
        }
        public string SerNameTh
        {
            set { _serNameTh = value; }
            get { return _serNameTh; }
        }
        public string TitleEn
        {
            set { _titleEn = value; }
            get { return _titleEn; }
        }
        public string NameEn
        {
            set { _nameEn = value; }
            get { return _nameEn; }
        }
        public string SerNameEn
        {
            set { _serNameEn = value; }
            get { return _serNameEn; }
        }
        public string Gender
        {
            set { _gender = value; }
            get { return _gender; }
        }
        public string BirthDate
        {
            set { _birthDate = value; }
            get { return _birthDate; }
        }
        public string Address
        {
            set { _address = value; }
            get { return _address; }
        }
        public string Moo
        {
            set { _moo = value; }
            get { return _moo; }
        }
        public string Trok
        {
            set { _trok = value; }
            get { return _trok; }
        }
        public string Soi
        {
            set { _soi = value; }
            get { return _soi; }
        }
        public string Road
        {
            set { _road = value; }
            get { return _road; }
        }
        public string Tumbol
        {
            set { _tumbol = value; }
            get { return _tumbol; }
        }
        public string Amphure
        {
            set { _amphure = value; }
            get { return _amphure; }
        }
        public string Province
        {
            set { _province = value; }
            get { return _province; }
        }

        private string toStringNullable(object reader)
        {
            return (reader is DBNull ? "" : (string)reader);
        }
    }
}
