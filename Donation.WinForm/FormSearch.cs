﻿using Donation.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormSearch : Form
    {
        public FormSearch()
        {
            InitializeComponent();
        }

        private void FormSearch_Load(object sender, EventArgs e)
        {
            while(Program.Token == null)
            {
                Thread.Sleep(300);
            }
            Task.Run(() => LoadHistory());
        }
        List<History> TopicList { get; set; }
        async Task LoadHistory()
        {
            var response = await Program.Rest.SearchDonateHistory(Program.Token);
            if(response.status)
            {
                TopicList = response.data;
                this.Invoke(new MethodInvoker(() =>
                {
                    foreach(var each in response.data)
                    {
                        var newItem = new ListViewItem(each.donateItem ? "กดที่นี่" : string.Empty);

                        newItem.SubItems.Add(each.DateString);
                        newItem.SubItems.Add(each.Money);
                        newItem.SubItems.Add(each.Organization);

                        listView1.Items.Add(newItem);
                    }
                }));
            }


        }

        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else
                {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }

        private void listView1_ItemSelectionChanged(object sender, ListViewItemSelectionChangedEventArgs e)
        {
            try
            {
                var history = TopicList.ElementAt(e.ItemIndex);
                if(history.donateItem)
                {
                    Task.Run(()=>LoadHistoryItems(history.donateId));
                }

            }
            catch(Exception exc)
            {

            }
        }
        async Task LoadHistoryItems(string id)
        {
            var response = await Program.Rest.SearchDonateItemHistory(Program.Token, id);
            if(response.status)
            {
                Program.HistoryItemList = response.data;

                this.Invoke(new MethodInvoker(()=> 
                {
                    Program.ChangePage(this, Program.formSearchItem);
                }));

            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formSearchOrDonation);
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }

        private void FormSearch_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formSearchOrDonation);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }
    }
}
