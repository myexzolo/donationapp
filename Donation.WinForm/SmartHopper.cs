﻿using ITLlib;
using System;
using System.Text;
using System.Threading;

using System.Windows.Forms;

namespace Donation.WinForm
{
    public class SmartHopper
    {
        public static String ComPort  = System.Configuration.ConfigurationManager.AppSettings["ComPort"];
        public static byte SSP1       = byte.Parse(System.Configuration.ConfigurationManager.AppSettings["SSP1"]);
        public static byte SSP2       = byte.Parse(System.Configuration.ConfigurationManager.AppSettings["SSP2"]);
        // Variables
        SSPComms sspLib = new SSPComms();
        public static bool hopperRunning = false, NV11Running = false;
        public static volatile bool hopperConnecting;
        public static volatile bool NV11Connecting;

        private static CHopper Hopper; // Class to interface with the Hopper
        private static CNV11 NV11; // Class to interface with the NV11
        private static bool FormSetup = false;
        private static frmPayoutByDenom payoutByDenomFrm;
        delegate void OutputMessage(string s);

        
        //private static System.Windows.Forms.TextBox log;
        //private static StringBuilder log;
    
        public static bool StartHopper(System.Windows.Forms.Timer timer1,TextBox textBox1, TextBox tbCoinLevels, TextBox tbNotesStored, TextBox txtAmount, int coincount, int coinbaht, int bankcount, int bankbaht, int total)
        {
            bool flag = true;

            try
            {
                Hopper = new CHopper();
                NV11 = new CNV11();
                if (Hopper == null || NV11 == null)
                {
                    MessageBox.Show("Error with memory allocation, exiting", "ERROR");
                    Application.Exit();
                }

                Global.ComPort = ComPort;
                Global.Validator1SSPAddress = SSP1;
                Global.Validator2SSPAddress = SSP2;

                //MessageBox.Show(Global.ComPort);


                MainLoop(timer1, textBox1, tbCoinLevels, tbNotesStored, txtAmount, coincount, coinbaht, bankcount, bankbaht, total);
            }
            catch (Exception ex)
            {
                flag = false;
                Console.WriteLine(ex);
            }
            return flag;
        }

        public static void MainLoop(System.Windows.Forms.Timer timer1, TextBox log, TextBox tbCoinLevels, TextBox tbNotesStored, TextBox txtAmount, int coincount, int coinbaht, int bankcount, int bankbaht, int total)
        {
            try
            {
                Thread tNV11Rec = null, tHopRec = null;

                // Connect to the validators

                ConnectToNV11(log);
                ConnectToHopper(log);
                NV11.EnableValidator();
                Hopper.EnableValidator();

                // While application is still active
                while (!CHelpers.Shutdown)
                {
                    // Setup form layout on first run
                    if (!FormSetup)
                    {
                        //SetupFormLayout();
                        FormSetup = true;
                    }

                    // If the hopper is supposed to be running but the poll fails
                    if (hopperRunning && !Hopper.DoPollV3(log, tbCoinLevels, tbNotesStored, txtAmount, coincount, coinbaht, total))
                    {
                        Hopper.Reset(log);
                        //log.AppendText("Lost connection to SMART Hopper\r\n");
                        // If the other unit isn't running, refresh the port by closing it
                        //if (!NV11Running) LibraryHandler.ClosePort();
                        //hopperRunning = false;
                        //tHopRec = new Thread(() => ReconnectHopper(log));
                        //tHopRec.Start();
                    }

                    // If thNV11Runninge NV11 is supposed to be running but the poll fails
                    bool NV11Res = NV11.DoPollV3(log, tbCoinLevels, tbNotesStored, txtAmount, bankcount, bankbaht, total);
                    if (NV11Running && !NV11Res)
                    {

                        NV11.Reset(log);
                        log.AppendText("Lost connection to NV11\r\n");
                        // If the other unit isn't running, refresh the port by closing it
                        //if (!hopperRunning) LibraryHandler.ClosePort();
                        //NV11Running = false;
                        //tNV11Rec = new Thread(() => ReconnectNV11(log));
                        //tNV11Rec.Start();
                    }

                    UpdateUIV2(tbNotesStored, tbCoinLevels);
                  
                    timer1.Enabled = true;

                    while (timer1.Enabled)
                    {
                        Application.DoEvents();
                        Thread.Sleep(1); // Yield so windows can schedule other threads to run
                    }
                }
                //close com port
                LibraryHandler.ClosePort();

            }
            catch (Exception ex){
                Console.WriteLine(ex);
            }
            
        }

        static void UpdateUI()
        {
            // Get stored notes info from NV11
            //tbNotesStored.Text = NV11.GetStorageInfo();
            //string NV11GetStorageInfo = NV11.GetStorageInfo();

            // Get channel info from hopper
            //tbCoinLevels.Text = Hopper.GetChannelLevelInfo();
            //string HopperGetChannelLevelInfo = Hopper.GetChannelLevelInfo();



        }

        static void UpdateUIV2(TextBox tbNotesStored, TextBox tbCoinLevels)
        {
            // Get stored notes info from NV11
            tbNotesStored.Text = NV11.GetStorageInfo();
            //string NV11GetStorageInfo = NV11.GetStorageInfo();

            // Get channel info from hopper
            tbCoinLevels.Text = Hopper.GetChannelLevelInfo();
            //string HopperGetChannelLevelInfo = Hopper.GetChannelLevelInfo();

        }

        bool OpenComPort()
        {
            SSP_COMMAND cmd = new SSP_COMMAND();
            cmd.ComPort = Global.ComPort;
            cmd.BaudRate = 9600;
            cmd.Timeout = 500;
            return sspLib.OpenSSPComPort(cmd);
        }

        private static void reconnectionTimer_Tick(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.Timer)
            {
                System.Windows.Forms.Timer t = sender as System.Windows.Forms.Timer;
                t.Enabled = false;
            }
        }


        public static void ConnectToHopper(TextBox log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            Hopper.CommandStructure.ComPort = Global.ComPort;
            Hopper.CommandStructure.SSPAddress = Global.Validator2SSPAddress;
            Hopper.CommandStructure.BaudRate = 9600;
            Hopper.CommandStructure.Timeout = 1000;
            Hopper.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (log != null) log.AppendText("Trying connection to SMART Hopper\r\n");

                // turn encryption off for first stage
                Hopper.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (Hopper.OpenPort() && Hopper.NegotiateKeys(log))
                {
                    Hopper.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxHopperProtocolVersion();
                    if (maxPVersion >= 6)
                        Hopper.SetProtocolVersion(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    Hopper.HopperSetupRequest(log);
                    // Get serial number.
                    Hopper.GetSerialNumber(log);
                    // inhibits, this sets which channels can receive notes
                    Hopper.SetInhibits(log);
                    // set running to true so the hopper begins getting polled
                    hopperRunning = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }
        public static void ConnectToHopperV2(StringBuilder log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            Hopper.CommandStructure.ComPort = Global.ComPort;
            Hopper.CommandStructure.SSPAddress = Global.Validator2SSPAddress;
            Hopper.CommandStructure.BaudRate = 9600;
            Hopper.CommandStructure.Timeout = 1000;
            Hopper.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (log != null) log.AppendLine("Trying connection to SMART Hopper\r\n");

                // turn encryption off for first stage
                Hopper.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (Hopper.OpenPort() && Hopper.NegotiateKeysV2(log))
                {
                    Hopper.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxHopperProtocolVersion();
                    if (maxPVersion >= 6)
                        Hopper.SetProtocolVersionV2(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    Hopper.HopperSetupRequestV2(log);
                    // Get serial number.
                    Hopper.GetSerialNumberV2(log);
                    // inhibits, this sets which channels can receive notes
                    Hopper.SetInhibitsV2(log);
                    // set running to true so the hopper begins getting polled
                    hopperRunning = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }

        public static void ConnectToNV11(TextBox log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            NV11.CommandStructure.ComPort = Global.ComPort;
            NV11.CommandStructure.SSPAddress = Global.Validator1SSPAddress;
            NV11.CommandStructure.BaudRate = 9600;
            NV11.CommandStructure.Timeout = 1000;
            NV11.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (log != null) log.AppendText("Trying connection to NV11\r\n");

                // turn encryption off for first stage
                NV11.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (NV11.OpenPort() && NV11.NegotiateKeys(log))
                {
                    NV11.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxNV11ProtocolVersion();
                    if (maxPVersion >= 6)
                        NV11.SetProtocolVersion(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    NV11.ValidatorSetupRequest(log);
                    // Get serial number.
                    NV11.GetSerialNumber(log);
                    // inhibits, this sets which channels can receive notes
                    NV11.SetInhibits(log);
                    // enable payout to begin with
                    NV11.EnablePayout(log);
                    // check for any stored notes
                    NV11.CheckForStoredNotes(log);
                    // report by the 4 byte value of the note, not the channel
                    NV11.SetValueReportingType(false, log);
                    // set running to true so the NV11 begins getting polled
                    NV11Running = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }
        public static void ConnectToNV11V2(StringBuilder log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            NV11.CommandStructure.ComPort = Global.ComPort;
            NV11.CommandStructure.SSPAddress = Global.Validator1SSPAddress;
            NV11.CommandStructure.BaudRate = 9600;
            NV11.CommandStructure.Timeout = 1000;
            NV11.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (log != null) log.Append("Trying connection to NV11\r\n");

                // turn encryption off for first stage
                NV11.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (NV11.OpenPort() && NV11.NegotiateKeysV2(log))
                {
                    NV11.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxNV11ProtocolVersion();
                    if (maxPVersion >= 6)
                        NV11.SetProtocolVersion2(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    NV11.ValidatorSetupRequestV2(log);
                    // Get serial number.
                    NV11.GetSerialNumberV2(log);
                    // inhibits, this sets which channels can receive notes
                    NV11.SetInhibitsV2(log);
                    // enable payout to begin with
                    NV11.EnablePayoutV2(log);
                    // check for any stored notes
                    NV11.CheckForStoredNotesV2(log);
                    // report by the 4 byte value of the note, not the channel
                    NV11.SetValueReportingTypeV2(false, log);
                    // set running to true so the NV11 begins getting polled
                    NV11Running = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }
        private static byte FindMaxHopperProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                Hopper.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (Hopper.CommandStructure.ResponseData[0] == CCommands.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }

        private static byte FindMaxNV11ProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                NV11.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (NV11.CommandStructure.ResponseData[0] == CCommands.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }

        private static void ReconnectHopper(TextBox log)
        {
            //OutputMessage m = new OutputMessage(AppendToTextBox);
            hopperConnecting = true;
            while (!hopperRunning)
            {
                //log.AppendText("Attempting to reconnect to SMART Hopper...\r\n");

                ConnectToHopper();
                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
                //log.AppendText("Reconnected to SMART Hopper\r\n");
            Hopper.EnableValidator();
            hopperConnecting = false;
        }

        public static void AppendToTextBox(string s,TextBox log)
        {
            log.AppendText(s);
        }

        private static void ReconnectNV11(TextBox log)
        {
            //OutputMessage m = new OutputMessage(AppendToTextBox);
            NV11Connecting = true;
            while (!NV11Running)
            {
                //log.AppendText("Attempting to reconnect to NV11...\r\n");
                ConnectToNV11(null); // Have to pass null as can't update text box from a different thread without invoking

                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
                //log.AppendText("Reconnected to NV11\r\n");
            NV11.EnableValidator();
            NV11Connecting = false;
        }
    }
}
