﻿namespace Donation.WinForm
{
    partial class FormDonate
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtAmount = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnPayoutByDenom = new System.Windows.Forms.Button();
            this.tbPayoutCurrency = new System.Windows.Forms.TextBox();
            this.tbCoinLevels = new System.Windows.Forms.TextBox();
            this.btnNoteFloatStackAll = new System.Windows.Forms.Button();
            this.btnResetNoteFloat = new System.Windows.Forms.Button();
            this.btnResetHopper = new System.Windows.Forms.Button();
            this.btnSmartEmptyHopper = new System.Windows.Forms.Button();
            this.btnStackNextNote = new System.Windows.Forms.Button();
            this.btnPayoutNextNote = new System.Windows.Forms.Button();
            this.btnPayout = new System.Windows.Forms.Button();
            this.btnEmptyHopper = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.tbPayout = new System.Windows.Forms.TextBox();
            this.btnHalt = new System.Windows.Forms.Button();
            this.cbRecycleChannelNV11 = new System.Windows.Forms.ComboBox();
            this.btnRun = new System.Windows.Forms.Button();
            this.tbNotesStored = new System.Windows.Forms.TextBox();
            this.logTickBox = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            this.ReturnNote = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.txtAmont = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button1000 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button200 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button500 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button100 = new System.Windows.Forms.Button();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(269, 1408);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(257, 54);
            this.label6.TabIndex = 12;
            this.label6.Text = "จำนวนเงินที่รับ";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(820, 1313);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(91, 54);
            this.label7.TabIndex = 13;
            this.label7.Text = "บาท";
            // 
            // txtAmount
            // 
            this.txtAmount.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAmount.Enabled = false;
            this.txtAmount.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmount.Location = new System.Drawing.Point(542, 1408);
            this.txtAmount.Name = "txtAmount";
            this.txtAmount.ReadOnly = true;
            this.txtAmount.Size = new System.Drawing.Size(259, 54);
            this.txtAmount.TabIndex = 15;
            this.txtAmount.Text = "0";
            this.txtAmount.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmount.TextChanged += new System.EventHandler(this.txtAmount_TextChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(821, 1408);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(91, 54);
            this.label8.TabIndex = 16;
            this.label8.Text = "บาท";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.Transparent;
            this.label9.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(818, 1502);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(91, 54);
            this.label9.TabIndex = 24;
            this.label9.Text = "บาท";
            // 
            // txtChange
            // 
            this.txtChange.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtChange.Enabled = false;
            this.txtChange.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChange.Location = new System.Drawing.Point(542, 1502);
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtChange.Size = new System.Drawing.Size(259, 54);
            this.txtChange.TabIndex = 23;
            this.txtChange.Text = "0";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.Color.Transparent;
            this.label10.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(270, 1502);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(259, 54);
            this.label10.TabIndex = 22;
            this.label10.Text = "จำนวนเงินทอน";
            // 
            // button8
            // 
            this.button8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button8.Font = new System.Drawing.Font("Superspace Bold", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button8.Location = new System.Drawing.Point(612, 699);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(164, 77);
            this.button8.TabIndex = 25;
            this.button8.Text = "ย้อนกลับ";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click_1);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button9.Font = new System.Drawing.Font("Superspace Bold", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button9.Location = new System.Drawing.Point(423, 699);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(164, 77);
            this.button9.TabIndex = 26;
            this.button9.Text = "หน้าหลัก";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click_1);
            // 
            // timer1
            // 
            this.timer1.Interval = 250;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.label3.Font = new System.Drawing.Font("Superspace Bold", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label3.Location = new System.Drawing.Point(0, 362);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(1064, 64);
            this.label3.TabIndex = 28;
            this.label3.Text = "กรมส่งเสริมและพัฒนาคุณภาพชีวิตคนพิการ";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Superspace Bold", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label1.Location = new System.Drawing.Point(0, 273);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(1064, 77);
            this.label1.TabIndex = 27;
            this.label1.Text = "ระบบบริจาค";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // btnPayoutByDenom
            // 
            this.btnPayoutByDenom.Location = new System.Drawing.Point(297, 336);
            this.btnPayoutByDenom.Margin = new System.Windows.Forms.Padding(2);
            this.btnPayoutByDenom.Name = "btnPayoutByDenom";
            this.btnPayoutByDenom.Size = new System.Drawing.Size(160, 23);
            this.btnPayoutByDenom.TabIndex = 45;
            this.btnPayoutByDenom.Text = "Payout by Denomination";
            this.btnPayoutByDenom.UseVisualStyleBackColor = true;
            this.btnPayoutByDenom.Visible = false;
            this.btnPayoutByDenom.Click += new System.EventHandler(this.btnPayoutByDenom_Click);
            // 
            // tbPayoutCurrency
            // 
            this.tbPayoutCurrency.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.tbPayoutCurrency.Location = new System.Drawing.Point(360, 277);
            this.tbPayoutCurrency.Name = "tbPayoutCurrency";
            this.tbPayoutCurrency.Size = new System.Drawing.Size(51, 20);
            this.tbPayoutCurrency.TabIndex = 44;
            this.tbPayoutCurrency.Text = "THB";
            this.tbPayoutCurrency.Visible = false;
            // 
            // tbCoinLevels
            // 
            this.tbCoinLevels.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbCoinLevels.Location = new System.Drawing.Point(0, 71);
            this.tbCoinLevels.Multiline = true;
            this.tbCoinLevels.Name = "tbCoinLevels";
            this.tbCoinLevels.ReadOnly = true;
            this.tbCoinLevels.Size = new System.Drawing.Size(161, 116);
            this.tbCoinLevels.TabIndex = 34;
            this.tbCoinLevels.Visible = false;
            // 
            // btnNoteFloatStackAll
            // 
            this.btnNoteFloatStackAll.Location = new System.Drawing.Point(539, 262);
            this.btnNoteFloatStackAll.Name = "btnNoteFloatStackAll";
            this.btnNoteFloatStackAll.Size = new System.Drawing.Size(121, 23);
            this.btnNoteFloatStackAll.TabIndex = 41;
            this.btnNoteFloatStackAll.Text = "Stack All";
            this.btnNoteFloatStackAll.UseVisualStyleBackColor = true;
            this.btnNoteFloatStackAll.Visible = false;
            this.btnNoteFloatStackAll.Click += new System.EventHandler(this.btnNoteFloatStackAll_Click);
            // 
            // btnResetNoteFloat
            // 
            this.btnResetNoteFloat.Location = new System.Drawing.Point(570, 389);
            this.btnResetNoteFloat.Name = "btnResetNoteFloat";
            this.btnResetNoteFloat.Size = new System.Drawing.Size(144, 23);
            this.btnResetNoteFloat.TabIndex = 40;
            this.btnResetNoteFloat.Text = "Reset NV11";
            this.btnResetNoteFloat.UseVisualStyleBackColor = true;
            this.btnResetNoteFloat.Visible = false;
            this.btnResetNoteFloat.Click += new System.EventHandler(this.btnResetNoteFloat_Click);
            // 
            // btnResetHopper
            // 
            this.btnResetHopper.Location = new System.Drawing.Point(420, 389);
            this.btnResetHopper.Name = "btnResetHopper";
            this.btnResetHopper.Size = new System.Drawing.Size(144, 23);
            this.btnResetHopper.TabIndex = 37;
            this.btnResetHopper.Text = "Reset Hopper";
            this.btnResetHopper.UseVisualStyleBackColor = true;
            this.btnResetHopper.Visible = false;
            this.btnResetHopper.Click += new System.EventHandler(this.btnResetHopper_Click);
            // 
            // btnSmartEmptyHopper
            // 
            this.btnSmartEmptyHopper.Location = new System.Drawing.Point(301, 164);
            this.btnSmartEmptyHopper.Name = "btnSmartEmptyHopper";
            this.btnSmartEmptyHopper.Size = new System.Drawing.Size(161, 23);
            this.btnSmartEmptyHopper.TabIndex = 33;
            this.btnSmartEmptyHopper.Text = "SMART Empty";
            this.btnSmartEmptyHopper.UseVisualStyleBackColor = true;
            this.btnSmartEmptyHopper.Visible = false;
            this.btnSmartEmptyHopper.Click += new System.EventHandler(this.btnSmartEmptyHopper_Click);
            // 
            // btnStackNextNote
            // 
            this.btnStackNextNote.Location = new System.Drawing.Point(630, 291);
            this.btnStackNextNote.Name = "btnStackNextNote";
            this.btnStackNextNote.Size = new System.Drawing.Size(121, 23);
            this.btnStackNextNote.TabIndex = 39;
            this.btnStackNextNote.Text = "Stack Next Note";
            this.btnStackNextNote.UseVisualStyleBackColor = true;
            this.btnStackNextNote.Visible = false;
            this.btnStackNextNote.Click += new System.EventHandler(this.btnStackNextNote_Click);
            // 
            // btnPayoutNextNote
            // 
            this.btnPayoutNextNote.Location = new System.Drawing.Point(539, 233);
            this.btnPayoutNextNote.Name = "btnPayoutNextNote";
            this.btnPayoutNextNote.Size = new System.Drawing.Size(121, 23);
            this.btnPayoutNextNote.TabIndex = 36;
            this.btnPayoutNextNote.Text = "Payout Next Note";
            this.btnPayoutNextNote.UseVisualStyleBackColor = true;
            this.btnPayoutNextNote.Visible = false;
            this.btnPayoutNextNote.Click += new System.EventHandler(this.btnPayoutNextNote_Click);
            // 
            // btnPayout
            // 
            this.btnPayout.Location = new System.Drawing.Point(297, 308);
            this.btnPayout.Name = "btnPayout";
            this.btnPayout.Size = new System.Drawing.Size(160, 23);
            this.btnPayout.TabIndex = 43;
            this.btnPayout.Text = "Payout";
            this.btnPayout.UseVisualStyleBackColor = true;
            this.btnPayout.Visible = false;
            this.btnPayout.Click += new System.EventHandler(this.btnPayout_Click);
            // 
            // btnEmptyHopper
            // 
            this.btnEmptyHopper.Location = new System.Drawing.Point(301, 193);
            this.btnEmptyHopper.Name = "btnEmptyHopper";
            this.btnEmptyHopper.Size = new System.Drawing.Size(161, 23);
            this.btnEmptyHopper.TabIndex = 31;
            this.btnEmptyHopper.Text = "Empty All to Cashbox";
            this.btnEmptyHopper.UseVisualStyleBackColor = true;
            this.btnEmptyHopper.Visible = false;
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox1.Location = new System.Drawing.Point(772, 3);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox1.Size = new System.Drawing.Size(280, 282);
            this.textBox1.TabIndex = 1;
            this.textBox1.Visible = false;
            // 
            // tbPayout
            // 
            this.tbPayout.Location = new System.Drawing.Point(360, 245);
            this.tbPayout.Name = "tbPayout";
            this.tbPayout.Size = new System.Drawing.Size(100, 20);
            this.tbPayout.TabIndex = 42;
            this.tbPayout.Visible = false;
            // 
            // btnHalt
            // 
            this.btnHalt.Location = new System.Drawing.Point(108, 385);
            this.btnHalt.Name = "btnHalt";
            this.btnHalt.Size = new System.Drawing.Size(75, 23);
            this.btnHalt.TabIndex = 35;
            this.btnHalt.Text = "&Halt";
            this.btnHalt.UseVisualStyleBackColor = true;
            this.btnHalt.Visible = false;
            this.btnHalt.Click += new System.EventHandler(this.btnHalt_Click);
            // 
            // cbRecycleChannelNV11
            // 
            this.cbRecycleChannelNV11.FormattingEnabled = true;
            this.cbRecycleChannelNV11.Location = new System.Drawing.Point(539, 177);
            this.cbRecycleChannelNV11.Name = "cbRecycleChannelNV11";
            this.cbRecycleChannelNV11.Size = new System.Drawing.Size(121, 21);
            this.cbRecycleChannelNV11.TabIndex = 30;
            this.cbRecycleChannelNV11.Visible = false;
            this.cbRecycleChannelNV11.SelectedIndexChanged += new System.EventHandler(this.cbRecycleChannelNV11_SelectedIndexChanged);
            // 
            // btnRun
            // 
            this.btnRun.Location = new System.Drawing.Point(15, 385);
            this.btnRun.Name = "btnRun";
            this.btnRun.Size = new System.Drawing.Size(75, 23);
            this.btnRun.TabIndex = 32;
            this.btnRun.Text = "&Run";
            this.btnRun.UseVisualStyleBackColor = true;
            this.btnRun.Visible = false;
            this.btnRun.Click += new System.EventHandler(this.btnRun_Click);
            // 
            // tbNotesStored
            // 
            this.tbNotesStored.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.tbNotesStored.Location = new System.Drawing.Point(179, 71);
            this.tbNotesStored.Multiline = true;
            this.tbNotesStored.Name = "tbNotesStored";
            this.tbNotesStored.ReadOnly = true;
            this.tbNotesStored.Size = new System.Drawing.Size(121, 116);
            this.tbNotesStored.TabIndex = 29;
            this.tbNotesStored.Visible = false;
            // 
            // logTickBox
            // 
            this.logTickBox.AutoSize = true;
            this.logTickBox.Location = new System.Drawing.Point(214, 389);
            this.logTickBox.Name = "logTickBox";
            this.logTickBox.Size = new System.Drawing.Size(81, 17);
            this.logTickBox.TabIndex = 46;
            this.logTickBox.Text = "Comms Log";
            this.logTickBox.UseVisualStyleBackColor = true;
            this.logTickBox.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(301, 237);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 26);
            this.label2.TabIndex = 47;
            this.label2.Text = "Amount\r\nto Payout:";
            this.label2.Visible = false;
            // 
            // ReturnNote
            // 
            this.ReturnNote.Location = new System.Drawing.Point(678, 233);
            this.ReturnNote.Name = "ReturnNote";
            this.ReturnNote.Size = new System.Drawing.Size(121, 23);
            this.ReturnNote.TabIndex = 41;
            this.ReturnNote.Text = "ReturnNote";
            this.ReturnNote.UseVisualStyleBackColor = true;
            this.ReturnNote.Visible = false;
            this.ReturnNote.Click += new System.EventHandler(this.ReturnNote_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.Color.Transparent;
            this.label15.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(821, 1313);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(91, 54);
            this.label15.TabIndex = 24;
            this.label15.Text = "บาท";
            // 
            // txtAmont
            // 
            this.txtAmont.BackColor = System.Drawing.SystemColors.Control;
            this.txtAmont.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.txtAmont.Cursor = System.Windows.Forms.Cursors.No;
            this.txtAmont.Enabled = false;
            this.txtAmont.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAmont.Location = new System.Drawing.Point(542, 1313);
            this.txtAmont.Name = "txtAmont";
            this.txtAmont.ReadOnly = true;
            this.txtAmont.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.txtAmont.Size = new System.Drawing.Size(259, 54);
            this.txtAmont.TabIndex = 14;
            this.txtAmont.Text = "0";
            this.txtAmont.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtAmont.TextChanged += new System.EventHandler(this.txtAmont_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(94, 1313);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(432, 54);
            this.label5.TabIndex = 11;
            this.label5.Text = "จำนวนเงินต้องการบริจาค";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(788, 41);
            this.panel1.TabIndex = 48;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // serialPort1
            // 
            this.serialPort1.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(this.serialPort1_DataReceived);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(83, 455);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(100, 20);
            this.textBox2.TabIndex = 51;
            this.textBox2.Visible = false;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.pictureBox4);
            this.panel2.Controls.Add(this.label11);
            this.panel2.Controls.Add(this.label4);
            this.panel2.Controls.Add(this.pictureBox3);
            this.panel2.Location = new System.Drawing.Point(630, 71);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(416, 608);
            this.panel2.TabIndex = 53;
            this.panel2.Visible = false;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::Donation.WinForm.Properties.Resources.close_window_xxl;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox4.Location = new System.Drawing.Point(722, 655);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(50, 50);
            this.pictureBox4.TabIndex = 3;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Superspace Light", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label11.Location = new System.Drawing.Point(20, 253);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(744, 106);
            this.label11.TabIndex = 2;
            this.label11.Text = "กรณีบริจาคเงินด้วยธนบัตร 500 บาทขึ้นไป \r\nระบบไม่สามารถทอนเงินได้";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Superspace Bold", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label4.ForeColor = System.Drawing.Color.Red;
            this.label4.Location = new System.Drawing.Point(175, 46);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(512, 73);
            this.label4.TabIndex = 1;
            this.label4.Text = "การแจ้งเตือนจากระบบ";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::Donation.WinForm.Properties.Resources.warning_icon;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox3.Location = new System.Drawing.Point(69, 37);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(100, 100);
            this.pictureBox3.TabIndex = 0;
            this.pictureBox3.TabStop = false;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.label13);
            this.panel3.Controls.Add(this.pictureBox6);
            this.panel3.Controls.Add(this.label12);
            this.panel3.Controls.Add(this.pictureBox5);
            this.panel3.Location = new System.Drawing.Point(183, 712);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(704, 361);
            this.panel3.TabIndex = 55;
            this.panel3.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Superspace Light", 32.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label13.Location = new System.Drawing.Point(28, 176);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(623, 96);
            this.label13.TabIndex = 5;
            this.label13.Text = "จำนวนเงินทอนไม่เพียงพอ \r\nกรุณาเตรียมเงินบริจาคให้พอดีกับที่บริจาค";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::Donation.WinForm.Properties.Resources.close_window_xxl;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox6.Location = new System.Drawing.Point(647, 304);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(50, 50);
            this.pictureBox6.TabIndex = 4;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Superspace Bold", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.label12.ForeColor = System.Drawing.Color.Red;
            this.label12.Location = new System.Drawing.Point(129, 55);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(512, 73);
            this.label12.TabIndex = 2;
            this.label12.Text = "การแจ้งเตือนจากระบบ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::Donation.WinForm.Properties.Resources.warning_icon;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox5.Location = new System.Drawing.Point(20, 33);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(100, 100);
            this.pictureBox5.TabIndex = 1;
            this.pictureBox5.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox2.Image = global::Donation.WinForm.Properties.Resources.brand;
            this.pictureBox2.Location = new System.Drawing.Point(432, 35);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(200, 200);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 50;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Image = global::Donation.WinForm.Properties.Resources.letter;
            this.pictureBox1.Location = new System.Drawing.Point(279, 1654);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(65, 64);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 49;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.Transparent;
            this.button7.BackgroundImage = global::Donation.WinForm.Properties.Resources._02;
            this.button7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.Enabled = false;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Superspace Bold", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(385, 1635);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(288, 100);
            this.button7.TabIndex = 21;
            this.button7.Tag = "100";
            this.button7.Text = "บริจาค";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.Transparent;
            this.button6.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(569, 1062);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(290, 130);
            this.button6.TabIndex = 17;
            this.button6.Tag = "100";
            this.button6.Text = "ไม่ระบุ\r\nจำนวน";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button1000
            // 
            this.button1000.BackColor = System.Drawing.Color.Transparent;
            this.button1000.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button1000.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button1000.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1000.FlatAppearance.BorderSize = 0;
            this.button1000.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button1000.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button1000.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1000.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1000.ForeColor = System.Drawing.Color.White;
            this.button1000.Location = new System.Drawing.Point(569, 888);
            this.button1000.Name = "button1000";
            this.button1000.Size = new System.Drawing.Size(290, 130);
            this.button1000.TabIndex = 17;
            this.button1000.Tag = "1,000";
            this.button1000.Text = "\"1,000\"\r\nบาท";
            this.button1000.UseVisualStyleBackColor = false;
            this.button1000.Click += new System.EventHandler(this.number_Click);
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.Transparent;
            this.button20.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button20.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Location = new System.Drawing.Point(569, 535);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(290, 130);
            this.button20.TabIndex = 17;
            this.button20.Tag = "20";
            this.button20.Text = "\"20\"\r\nบาท";
            this.button20.UseVisualStyleBackColor = false;
            this.button20.Click += new System.EventHandler(this.number_Click);
            // 
            // button200
            // 
            this.button200.BackColor = System.Drawing.Color.Transparent;
            this.button200.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button200.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button200.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button200.FlatAppearance.BorderSize = 0;
            this.button200.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button200.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button200.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button200.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button200.ForeColor = System.Drawing.Color.White;
            this.button200.Location = new System.Drawing.Point(569, 712);
            this.button200.Name = "button200";
            this.button200.Size = new System.Drawing.Size(290, 130);
            this.button200.TabIndex = 17;
            this.button200.Tag = "200";
            this.button200.Text = "\"200\"\r\nบาท";
            this.button200.UseVisualStyleBackColor = false;
            this.button200.Click += new System.EventHandler(this.number_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.Transparent;
            this.button5.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(201, 1062);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(290, 130);
            this.button5.TabIndex = 17;
            this.button5.Tag = "100";
            this.button5.Text = "ระบุ\r\nจำนวน";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click_2);
            // 
            // button500
            // 
            this.button500.BackColor = System.Drawing.Color.Transparent;
            this.button500.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button500.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button500.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button500.FlatAppearance.BorderSize = 0;
            this.button500.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button500.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button500.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button500.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button500.ForeColor = System.Drawing.Color.White;
            this.button500.Location = new System.Drawing.Point(201, 888);
            this.button500.Name = "button500";
            this.button500.Size = new System.Drawing.Size(290, 130);
            this.button500.TabIndex = 17;
            this.button500.Tag = "500";
            this.button500.Text = "\"500\"\r\nบาท";
            this.button500.UseVisualStyleBackColor = false;
            this.button500.Click += new System.EventHandler(this.number_Click);
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.Transparent;
            this.button10.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Location = new System.Drawing.Point(201, 535);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(290, 130);
            this.button10.TabIndex = 1;
            this.button10.Tag = "10";
            this.button10.Text = "\"10\"\r\nบาท";
            this.button10.UseVisualStyleBackColor = false;
            this.button10.Click += new System.EventHandler(this.number_Click);
            // 
            // button100
            // 
            this.button100.BackColor = System.Drawing.Color.Transparent;
            this.button100.BackgroundImage = global::Donation.WinForm.Properties.Resources._01;
            this.button100.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.button100.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button100.FlatAppearance.BorderSize = 0;
            this.button100.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.button100.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.button100.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button100.Font = new System.Drawing.Font("Superspace Bold", 35.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button100.ForeColor = System.Drawing.Color.White;
            this.button100.Location = new System.Drawing.Point(201, 712);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(290, 130);
            this.button100.TabIndex = 17;
            this.button100.Tag = "100";
            this.button100.Text = "\"100\"\r\nบาท";
            this.button100.UseVisualStyleBackColor = false;
            this.button100.Click += new System.EventHandler(this.number_Click);
            // 
            // FormDonate
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(206)))), ((int)(((byte)(226)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(788, 788);
            this.ControlBox = false;
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.logTickBox);
            this.Controls.Add(this.btnPayoutByDenom);
            this.Controls.Add(this.tbPayoutCurrency);
            this.Controls.Add(this.tbCoinLevels);
            this.Controls.Add(this.ReturnNote);
            this.Controls.Add(this.btnNoteFloatStackAll);
            this.Controls.Add(this.btnResetNoteFloat);
            this.Controls.Add(this.btnResetHopper);
            this.Controls.Add(this.btnSmartEmptyHopper);
            this.Controls.Add(this.btnStackNextNote);
            this.Controls.Add(this.btnPayoutNextNote);
            this.Controls.Add(this.btnPayout);
            this.Controls.Add(this.btnEmptyHopper);
            this.Controls.Add(this.tbPayout);
            this.Controls.Add(this.btnHalt);
            this.Controls.Add(this.cbRecycleChannelNV11);
            this.Controls.Add(this.btnRun);
            this.Controls.Add(this.tbNotesStored);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtChange);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button1000);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button200);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button500);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.button100);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtAmount);
            this.Controls.Add(this.txtAmont);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximizeBox = false;
            this.Name = "FormDonate";
            this.TopMost = true;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FormDonate_FormClosing);
            this.Load += new System.EventHandler(this.FormDonate_Load);
            this.Shown += new System.EventHandler(this.FormDonate_Shown);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtAmount;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Button button200;
        private System.Windows.Forms.Button button500;
        private System.Windows.Forms.Button button1000;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtChange;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnPayoutByDenom;
        private System.Windows.Forms.TextBox tbPayoutCurrency;
        private System.Windows.Forms.TextBox tbCoinLevels;
        private System.Windows.Forms.Button btnNoteFloatStackAll;
        private System.Windows.Forms.Button btnResetNoteFloat;
        private System.Windows.Forms.Button btnResetHopper;
        private System.Windows.Forms.Button btnSmartEmptyHopper;
        private System.Windows.Forms.Button btnStackNextNote;
        private System.Windows.Forms.Button btnPayoutNextNote;
        private System.Windows.Forms.Button btnPayout;
        private System.Windows.Forms.Button btnEmptyHopper;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox tbPayout;
        private System.Windows.Forms.Button btnHalt;
        private System.Windows.Forms.ComboBox cbRecycleChannelNV11;
        private System.Windows.Forms.Button btnRun;
        private System.Windows.Forms.TextBox tbNotesStored;
        private System.Windows.Forms.CheckBox logTickBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button ReturnNote;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtAmont;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel1;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox pictureBox5;
    }
}