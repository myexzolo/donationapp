﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Donation.Models;
using ITLlib;

namespace Donation.WinForm
{
    public partial class FormDonate : Form
    {
        public String ComPort = System.Configuration.ConfigurationManager.AppSettings["ComPort"];
        public String PortName = System.Configuration.ConfigurationManager.AppSettings["ComPortNum"];
        public string BaudRate = System.Configuration.ConfigurationManager.AppSettings["BaudRate"];
        public string DataBits = System.Configuration.ConfigurationManager.AppSettings["DataBits"];
        public string Parity = System.Configuration.ConfigurationManager.AppSettings["Parity"];
        public string StopBits = System.Configuration.ConfigurationManager.AppSettings["StopBits"];
        public byte SSP1 = byte.Parse(System.Configuration.ConfigurationManager.AppSettings["SSP1"]);
        public byte SSP2 = byte.Parse(System.Configuration.ConfigurationManager.AppSettings["SSP2"]);

        public String RecycleBank = System.Configuration.ConfigurationManager.AppSettings["RecycleBank"];

        bool typeConate = false;
        bool openhopper = false;
        bool RecycleBankSet = true;

        private Thread runThread = null;
        CaptureCamera cam = new CaptureCamera();

        SSPComms sspLib = new SSPComms();
        public bool hopperRunning = false, NV11Running = false;
        public volatile bool hopperConnecting;
        public volatile bool NV11Connecting;

        int pollTimer = 250; // timer in ms
        CHopper Hopper; // Class to interface with the Hopper
        CNV11 NV11; // Class to interface with the NV11
        bool FormSetup = false;
        delegate void OutputMessage(string s);

        FrmLoading frmLoading;


        public FormDonate()
        {
            //Thread thread = new Thread(() => showLoading());
            //thread.Start();
            InitializeComponent();
        }

        public void showLoading()
        {
            frmLoading = new FrmLoading();
            frmLoading.Show();
        }


        public void displayScreens()
        {
            try
            {
                GC.Collect();
                Screen[] screens = Screen.AllScreens;
                if (screens.Length > 1)
                {
                    int screensNum = 1;
                    Rectangle bounds = screens[screensNum].Bounds;
                    if (bounds.X != this.Left)
                    {
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {

                                this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                                this.FormBorderStyle = FormBorderStyle.None;
                                this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                                this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                                this.MaximumSize = new Size(bounds.Width, bounds.Height);
                                this.WindowState = FormWindowState.Normal;
                            }));
                        }
                        else
                        {
                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.WindowState = FormWindowState.Normal;
                        }
                    }
                }
                else {
                    Rectangle bounds = screens[0].Bounds;
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {

                            this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                            this.FormBorderStyle = FormBorderStyle.None;
                            this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                            this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                            this.MaximumSize = new Size(bounds.Width, bounds.Height);
                            this.WindowState = FormWindowState.Normal;
                        }));
                    }
                    else
                    {
                        this.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
                        this.FormBorderStyle = FormBorderStyle.None;
                        this.Size = new Size(bounds.Width + 10, bounds.Height + 10);
                        this.MaximumSize = new Size(bounds.Width, bounds.Height);
                        this.StartPosition = FormStartPosition.WindowsDefaultLocation;
                        this.WindowState = FormWindowState.Normal;
                    }
                }
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog(":: checkDisplayScreens ::" + ex.ToString(), "DispalyQueueHorizontal");
            }
        }


        private void FormDonate_Load(object sender, EventArgs e)
        {
            displayScreens();
            try
            {
                CHelpers.Shutdown = false;
                this.runThread = new Thread(new ThreadStart(this.runDonate));
                this.runThread.SetApartmentState(ApartmentState.STA);
                this.runThread.Start();

                try
                {
                    if (cam.filters != null) {
                        cam.Show();
                    }
                    
                }
                catch (Exception ex) {
                    ;
                }
                
                timer1.Interval = pollTimer;
                btnHalt.Enabled = false;

                Global.ComPort = ComPort;
                Global.Validator1SSPAddress = SSP1;
                Global.Validator2SSPAddress = SSP2;

                Global.coinbaht     = 0;
                Global.coincount    = 0;
                Global.bankcount    = 0;
                Global.bankbaht     = 0;
                Global.total        = 0;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
            
        }

        private void runDonate()
        {
            try
            {
                Hopper = new CHopper();
                NV11 = new CNV11();
                if (Hopper == null || NV11 == null)
                {
                    MessageBox.Show("Error with memory allocation, exiting", "ERROR");
                    Application.Exit();
                }
                btnRun_Click(null, null);
                typeConate = false;
                

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }
        bool ChangeIsNotEnought()
        {
            //typeConate = true;

            int amount  = Int32.Parse(txtAmont.Text.Replace(",", ""));
            int num     = Int32.Parse(txtAmount.Text.Replace(",", ""));
            int change  = Int32.Parse(txtChange.Text.Replace(",", ""));

            int x1 = (Global.coin1 * 1);
            int x2 = (Global.coin2 * 2);
            int x5 = (Global.coin5 * 5);
            int x10 = (Global.coin10 * 10);
            int x100 = (Global.bankRecycle * 100);

            if ((x1 + x2 + x5 + x10) < amount && amount <= 100 && change > 0)
            {
                panel3.Visible = true;
            }
            else if (x100 < amount && amount <= 500 && amount > 100 && change > 0)
            {
                panel3.Visible = true;
            }
            else if (x100 < amount && amount <= 1000 && amount > 100 && change > 0)
            {
                panel3.Visible = true;
            }

            return panel3.Visible;

        }

        public void runAudio2(String s)
        {
            playWave(s);
        }

        public void runAudio3(String s1, String s2)
        {
            playWave(s1);
            playWave(s2);
        }


        public void runAudio()
        {
            playWave("selectDepartment");
        }

        private void playWave(string s)
        {
            GC.Collect();
            WAVPlayer ws = new WAVPlayer();
            string soundPath = Directory.GetCurrentDirectory() + @"\SOUND\" + s + @".wav";
            ws.PlayAudio(soundPath);

        }

        private void number_Click(object sender, EventArgs e)
        {
            
            Program.RefreshTime();
            typeConate = true;
            txtAmont.Text = ((Button)sender).Tag.ToString();

            int amount = Int32.Parse(txtAmont.Text.Replace(",", ""));
            int num = Int32.Parse(txtAmount.Text.Replace(",", ""));

            runAudio3(amount.ToString(), "bath");

            int x1 = (Global.coin1 * 1);
            int x2 = (Global.coin2 * 2);
            int x5 = (Global.coin5 * 5);
            int x10 = (Global.coin10 * 10);
            int x100 = (Global.bankRecycle * 100);

            if ((x1 + x2 + x5 + x10) < amount && amount <= 100)
            {
                //panel3.Visible = true;
            }
            else if (x100 < amount && amount <= 500 && amount > 100)
            {
                //panel3.Visible = true;
            }
            else if (x100 < amount && amount <= 1000 && amount > 100)
            {
                //panel3.Visible = true;
            }

            cal();
            if (!openhopper) {
                openhopper = true;
            }
        }

        private void cal() {
            if (typeConate)
            {
                int amount = Int32.Parse(txtAmont.Text.Replace(",", ""));
                int num = Int32.Parse(txtAmount.Text.Replace(",", ""));
                if (num >= amount)
                {
                    int cal = (num - amount);
                    int x1  = (Global.coin1 * 1);
                    int x2  = (Global.coin2 * 2);
                    int x5  = (Global.coin5 * 5);
                    int x10 = (Global.coin10 * 10);
                    int x100 = (Global.bankRecycle * 100);

                    if ((x1 + x2 + x5 + x10) < cal)
                    {
                        panel3.Visible = true;

                        
                        if (this.InvokeRequired)
                        {
                            this.Invoke(new MethodInvoker(delegate
                            {
                                txtAmont.Text = txtAmount.Text;
                                cal = 0;
                            }));
                        }
                        else
                        {
                            txtAmont.Text = txtAmount.Text;
                            cal = 0;
                        }

                    }
                    else if(x100 < cal && cal < 500 && cal > 100)
                    {
                        //panel3.Visible = true;
                    }
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            txtChange.Text = cal.ToString();
                        }));
                    }
                    else
                    {
                        txtChange.Text = cal.ToString();
                    }

                    button7.Enabled = true;
                    button8.Enabled = false;
                    button9.Enabled = false;
                    //hopperRunning = false;
                    //NV11Running = false;
                }
                else
                {
                    if (this.InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            txtChange.Text = "0";
                        }));
                    }
                    else
                    {
                        txtChange.Text = "0";
                    }

                    button7.Enabled = false;
                    button8.Enabled = true;
                    button9.Enabled = true;
                }
            }
            else
            {
                txtAmont.Text = txtAmount.Text;
                if (this.InvokeRequired)
                {
                    this.Invoke(new MethodInvoker(delegate
                    {
                        txtChange.Text = "0";
                    }));
                }
                else
                {
                    txtChange.Text = "0";
                }

            }

        }

        private void number2_Click(object sender, EventArgs e)
        {
            SetAmount(((Button)sender).Tag.ToString());
        }

        private void donate_Click(object sender, EventArgs e)
        {
            SetAmount(((Button)sender).Tag.ToString());
        }

        private void startDonate() {
            
        }
        

        private void SetAmount(string number)
        {
            try
            {
                if (txtAmont.InvokeRequired)
                {
                    txtAmont.Invoke(new MethodInvoker(delegate {
                        int index = txtAmont.SelectionStart;
                        Console.WriteLine(index);
                        if (number.Equals("*")) {
                            txtAmont.Text = "";
                        }
                        else if (number.Equals("#")) {
                            int textlength = txtAmont.Text.Length;
                            if (textlength > 0 && index > 0)
                            {
                                txtAmont.Text = txtAmont.Text.Remove(txtAmont.SelectionStart - 1, 1);
                                txtAmont.Select(index - 1, 1);
                            }
                            else if (index == 0)
                            {
                                txtAmont.Text = "";
                            }
                            else
                            {
                                //manualKey = false;
                            }
                        }
                        else {
                            txtAmont.Text = txtAmont.Text.Insert(index, number);

                            //txtAmont.Text = txtAmont.Text + number.ToString();

                            txtAmont.Focus();

                            if (index < txtAmont.Text.Length - 1)
                            {
                                txtAmont.SelectionStart = index - 1;
                            }
                            else
                            {
                                txtAmont.SelectionStart = txtAmont.Text.Length;
                            }
                        }
                        cal();
                        //txtAmont.Focus();
                        //txtAmont.SelectionStart = index;
                        //txtAmont.SelectionLength = 0;
                    }));
                }
                else
                {
                    int index = txtAmont.SelectionStart - 1;
                    Console.WriteLine(index);
                    if (number.Equals("*"))
                    {
                        txtAmont.Text = "";
                    }
                    else if (number.Equals("#"))
                    {
                        int textlength = txtAmont.Text.Length;
                        if (textlength > 0 && index > 0)
                        {
                            txtAmont.Text = txtAmont.Text.Remove(txtAmont.SelectionStart - 1, 1);
                            txtAmont.Select(index - 1, 1);
                        }
                        else if (index == 0)
                        {
                            txtAmont.Text = "";
                        }
                        else
                        {
                            //manualKey = false;
                        }
                    }
                    else
                    {
                        txtAmont.Text = txtAmont.Text.Insert(index, number);

                        txtAmont.Focus();

                        if (index < txtAmont.Text.Length - 1)
                        {
                            txtAmont.SelectionStart = index + 1;
                        }
                        else
                        {
                            txtAmont.SelectionStart = txtAmont.Text.Length;
                        }
                    }
                    cal();
                    //txtAmont.Focus();
                    //txtAmont.SelectionStart = index;
                    //txtAmont.SelectionLength = 0;
                }                
            }
            catch (Exception e) {

            }
        }

        public bool OpenPort()
        {
            bool error = false;
            bool comportIsOpen = true;

            if (serialPort1.IsOpen) serialPort1.Close();
            else
            {
                serialPort1.PortName = PortName;//settings.PortName;
                serialPort1.BaudRate = Int32.Parse(BaudRate);//settings.BaudRate;
                serialPort1.DataBits = Int32.Parse(DataBits);//settings.DataBits;
                serialPort1.StopBits = (StopBits)Enum.Parse(typeof(StopBits), StopBits);
                serialPort1.Parity = (Parity)Enum.Parse(typeof(Parity), Parity);
                serialPort1.RtsEnable = true;

                try
                {
                    serialPort1.Open();
                }
                catch (UnauthorizedAccessException) { error = true; }
                catch (IOException) { error = true; }
                catch (ArgumentException) { error = true; }

                if (error)
                {
                    comportIsOpen = false;
                    MessageBox.Show("Could not open the COM port.");
                    //Utils.getErrorToLog("Could not open the COM port.  Most likely it is already in use, has been removed, or is unavailable.", "IQUEUE_DISPLAY.Utils");
                }
            }
            return comportIsOpen;
        }

        public void ClosePort()
        {
            if (serialPort1.IsOpen) serialPort1.Close();

        }

        private void serialPort1_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                //Thread.Sleep(1000);
                int length = serialPort1.BytesToRead;
                byte[] buf = new byte[length];

                serialPort1.Read(buf, 0, length);
                System.Diagnostics.Debug.WriteLine("Received Data:" + buf);

                string result = System.Text.Encoding.UTF8.GetString(buf);
                //String str = "";
                //foreach (char c in buf)
                //{
                //    str += String.Format("{0:x2}", (byte)c);
                //}
                result = result.Replace(":K", "");
                result = result.Replace("K", "");
                result = result.Replace(":", "");
                result = result.Replace("\u0003", "");


                if (int.TryParse(result, out int n))
                {
                    SetAmount(result);
                } else if (result == "*") {
                    SetAmount(result); 
                } else if (result == "#") {
                    SetAmount(result);
                } 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                //Utils.getErrorToLog(":: serialPort1_DataReceived ::" + ex.ToString(), "IQUEUE_KEYPAD");
            }
        }

        private void btnDel_Click(object sender, EventArgs e)
        {/*
            int textlength = txtAmont.Text.Length;
            int index = txtAmont.SelectionStart;

            if (textlength > 0 && index > 0)
            {
                txtAmont.Text = txtAmont.Text.Remove(txtAmont.SelectionStart - 1, 1);
                txtAmont.Select(index - 1, 1);
            }
            else if (index == 0)
            {
                txtAmont.Text = "";
            }
            else
            {
                //manualKey = false;
            }
            txtAmont.Focus();
            txtAmont.SelectionStart = index;
            txtAmont.SelectionLength = 0;*/
        }

        private void FormDonate_FormClosing(object sender, FormClosingEventArgs e)
        {
            NV11Running = false;
            hopperRunning = false;
            CHelpers.Shutdown = true;
            Global.Comms = logTickBox.Checked;
            this.Dispose();


            try
            {
                if (cam != null)
                {
                    cam.Close();
                    cam.Dispose();
                    cam = null;
                }
            }
            catch {
                Console.WriteLine("Error Close Camera");
            }

            
            
        }

        private void button5_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formIdentity);
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formMoneyType);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Program.ChangePage(this, Program.formVdo);
        }

        private void button5_Click_1(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("NotSpecifyNumber"));
            thread.Start();
            Program.RefreshTime();
            typeConate = false;
            cal();

            string amountTxt = txtAmount.Text.Replace(",", "");
            if (int.Parse(amountTxt) > 0) {
               
                button7.Enabled = true;
            }
        }

        private void txtAmount_TextChanged(object sender, EventArgs e)
        {
            cal();

            if (!typeConate){
                string amountTxt = txtAmount.Text.Replace(",", "");
                if (int.Parse(amountTxt) > 0)
                {
                    button7.Enabled = true;
                }
            }

            //MessageBox.Show(Program.coincount +"," + Program.coinbaht + "," + Program.bankcount + "," + Program.bankbaht + "," + Program.total);

            Task.Run(() => Program.Rest.PiFront(new PiFrontParam
            {
                coincount = Global.coincount,
                coinbaht = Global.coinbaht,
                bankcount = Global.bankcount,
                bankbaht = Global.bankbaht,
                total = Global.total
            }));
        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button9_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (cam != null)
                {
                    cam.Close();
                    cam.Dispose();
                    cam = null;
                }
            }
            catch
            {
                Console.WriteLine("Error Close Camera");
            }
            Program.ChangePageV2(this, Program.formVdo);
        }

        private void button8_Click_1(object sender, EventArgs e)
        {
            try
            {
                if (cam != null)
                {
                    cam.Close();
                    cam.Dispose();
                    cam = null;
                    
                }
            }
            catch
            {
                Console.WriteLine("Error Close Camera");
            }
            Program.ChangePageV2(this, Program.formMoneyType2);

        }

        private void button7_Click(object sender, EventArgs e)
        {
            if (ChangeIsNotEnought())
            {
                //return;
            }
            string change = txtChange.Text.Replace(",", "");
            Task.Run(() => Program.Rest.PiFront(new PiFrontParam
            {
                bankbaht = 0,
                coinbaht = 0,
                coincount = 0,
                bankcount = 0,
                total = 0,
            }));



            Global.Allcoinbaht += Global.coinbaht;
            Global.Allbankbaht += Global.bankbaht;
            Global.AllTotal += Global.total;
            Global.coinbaht = 0;
            Global.coincount = 0;
            Global.bankbaht = 0;
            Global.bankcount = 0;
            Global.total = 0;

            //Task.Run(() => Program.DoDonate(200));
            Task.Run(() => Program.Rest.PiBack(new PiBackParam
            {
                coinbaht = Global.Allcoinbaht,
                bankbaht = Global.Allbankbaht,
                total = Global.AllTotal
            }));

            if (int.Parse(change) > 0)
            {
                Thread thread = new Thread(() => runAudio2("slipAndChange"));
                thread.Start();
                //int changeB = int.Parse(change);
                int changeB = 0;
                if (changeB >= 100)
                {
                    double val = (double.Parse(change) / 100);
                    string res = val.ToString("###.00");
                    string[] ma = res.Split('.');
                    int m = int.Parse(ma[0]);
                    int ms = int.Parse(ma[1]);

                    for (int x = 0; x < m; x++)
                    {
                        NV11.PayoutNextNote(textBox1);
                        //Thread.Sleep(3000);
                    }
                    tbPayout.Text = ms.ToString();
                    btnPayout_Click(sender, e);
                }
                else
                {
                    tbPayout.Text = change;
                    btnPayout_Click(sender, e);

                }
            }
            else
            {
                Thread thread = new Thread(() => runAudio2("slip"));
                thread.Start();
            }

           

            string donant = txtAmont.Text;
            if (!typeConate) donant = txtAmount.Text;
            FormBill formBill = new FormBill(donant);
            formBill.ShowDialog();

            txtAmont.Text = "0";
            txtAmount.Text = "0";                      
            txtChange.Text = "0";
            try
            {
                if (cam != null)
                {
                    cam.Close();
                    cam.Dispose();
                    cam = null;
                }
                ClosePort();
            }
            catch
            {
                Console.WriteLine("Error Close Camera");
            }

            Program.ChangePage(this, Program.formVdo);
        }

        private void btnRun_Click(object sender, EventArgs e)
        {

            if (this.InvokeRequired)
            {
                this.Invoke(new MethodInvoker(delegate
                {
                    textBox1.AppendText("Started poll loop\r\n");
                    MainLoop();
                }));
            }
            else
            {
                textBox1.AppendText("Started poll loop\r\n");
                MainLoop();
            }
            
        }

        public void MainLoop()
        {
            btnRun.Enabled = false;
            btnHalt.Enabled = true;
            Thread tNV11Rec = null, tHopRec = null;

            // Connect to the validators
            ConnectToNV11(textBox1);
            ConnectToHopper(textBox1);

           

            NV11.EnableValidator();
            Hopper.EnableValidator();

            


            // While application is still active
            while (!CHelpers.Shutdown)
            {
                try
                {
                    // Setup form layout on first run
                    if (!FormSetup)
                    {
                        SetupFormLayout();
                        FormSetup = true;
                    }

                    // If the hopper is supposed to be running but the poll fails
                    if (hopperRunning && !Hopper.DoPollV3(textBox1, txtAmount))
                    {
                        textBox1.AppendText("Lost connection to SMART Hopper\r\n");
                        // If the other unit isn't running, refresh the port by closing it
                        if (!NV11Running) LibraryHandler.ClosePort();
                        hopperRunning = false;
                        tHopRec = new Thread(() => ReconnectHopper());
                        tHopRec.Start();
                    }

                    // If the NV11 is supposed to be running but the poll fails
                    if (NV11Running && !NV11.DoPollV3(textBox1, txtAmount))
                    {
                        textBox1.AppendText("Lost connection to NV11\r\n");
                        // If the other unit isn't running, refresh the port by closing it
                        if (!hopperRunning) LibraryHandler.ClosePort();
                        NV11Running = false;
                        tNV11Rec = new Thread(() => ReconnectNV11());
                        tNV11Rec.Start();
                    }

                    UpdateUI();
                    timer1.Enabled = true;

                    while (timer1.Enabled)
                    {
                        Application.DoEvents();
                        Thread.Sleep(1); // Yield so windows can schedule other threads to run
                    }

                    if (RecycleBank != "" && RecycleBankSet)
                    {
                        RecycleBankSet = false;
                        NV11.RouteAllToStack();

                        // switch selected note to recycle after enabling payout
                        NV11.EnablePayout();
                        string[] sArr = RecycleBank.Split(' ');
                        try
                        {
                            NV11.ChangeNoteRoute(Int32.Parse(sArr[0]) * 100, sArr[1].ToCharArray(), false, textBox1);
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                            return;
                        }
                    }
                }
                catch(Exception exc)
                {
                    //MessageBox.Show("err");
                }
                
            }

            //close com port
            LibraryHandler.ClosePort();

            btnRun.Enabled = true;
            btnHalt.Enabled = false;
        }

        void UpdateUI()
        {
            // Get stored notes info from NV11
            tbNotesStored.Text = NV11.GetStorageInfo();
            //textBox2.Text = Global.bankRecycle.ToString();

            // Get channel info from hopper
            tbCoinLevels.Text = Hopper.GetChannelLevelInfo();
            //textBox2.Text = textBox2.Text + ", coin1 :" + Global.coin1 + ", coin2 :" + Global.coin2 + ", coin5 :" + Global.coin5 + ", coin10 :" + Global.coin10;
        }

        private void ReconnectHopper()
        {
            OutputMessage m = new OutputMessage(AppendToTextBox);
            hopperConnecting = true;
            while (!hopperRunning)
            {
                if (this.IsDisposed)
                {
                    break;
                }
                if (textBox1.InvokeRequired)
                    textBox1.Invoke(m, new object[] { "Attempting to reconnect to SMART Hopper...\r\n" });
                else
                    textBox1.AppendText("Attempting to reconnect to SMART Hopper...\r\n");

                ConnectToHopper();
                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
            if (textBox1.InvokeRequired)
                textBox1.Invoke(m, new object[] { "Reconnected to SMART Hopper\r\n" });
            else
                textBox1.AppendText("Reconnected to SMART Hopper\r\n");
            Hopper.EnableValidator();
            hopperConnecting = false;
        }

        private void ReconnectNV11()
        {
            OutputMessage m = new OutputMessage(AppendToTextBox);
            NV11Connecting = true;
            while (!NV11Running)
            {
                if (this.IsDisposed)
                {
                    break;
                }
                if (textBox1.InvokeRequired)
                    textBox1.Invoke(m, new object[] { "Attempting to reconnect to NV11...\r\n" });
                else
                    textBox1.AppendText("Attempting to reconnect to NV11...\r\n");

                ConnectToNV11(null); // Have to pass null as can't update text box from a different thread without invoking

                CHelpers.Pause(1000);
                if (CHelpers.Shutdown) return;
            }
            if (textBox1.InvokeRequired)
                textBox1.Invoke(m, new object[] { "Reconnected to NV11\r\n" });
            else
                textBox1.AppendText("Reconnected to NV11\r\n");
            NV11.EnableValidator();
            NV11Connecting = false;
        }

        public void AppendToTextBox(string s)
        {
            textBox1.AppendText(s);
        }



        private void SetupFormLayout()
        {
            // Note float UI setup

            // Find number and value of channels in NV11 and update combo box
            cbRecycleChannelNV11.Items.Add("No recycling");
            foreach (ChannelData d in NV11.UnitDataList)
            {
                cbRecycleChannelNV11.Items.Add(d.Value / 100 + " " + new String(d.Currency));
            }
            cbRecycleChannelNV11.SelectedIndex = 0;

            // Get channel levels in hopper
            tbCoinLevels.Text = Hopper.GetChannelLevelInfo();

            // setup list of recyclable channel tick boxes
            int x = 0, y = 0;
            x = 465;
            y = 30;

            Label lbl = new Label();
            lbl.Location = new Point(x, y);
            lbl.Size = new Size(70, 35);
            lbl.Name = "lbl";
            lbl.Text = "Recycle\nChannels:";
            Controls.Add(lbl);

            y += 20;
            for (int i = 1; i <= Hopper.NumberOfChannels; i++)
            {
                CheckBox c = new CheckBox();
                c.Location = new Point(x, y + (i * 20));
                c.Name = i.ToString();
                c.Text = CHelpers.FormatToCurrency(Hopper.GetChannelValue(i)) + " " + new String(Hopper.GetChannelCurrency(i));
                c.Checked = Hopper.IsChannelRecycling(i);
                c.CheckedChanged += new EventHandler(recycleBox_CheckedChange);
                Controls.Add(c);
            }
        }

        private void recycleBox_CheckedChange(object sender, EventArgs e)
        {
            CheckBox c;
            if (sender is CheckBox)
            {
                c = sender as CheckBox;
                try
                {
                    int n = Int32.Parse(c.Name);
                    if (c.Checked)
                        Hopper.RouteChannelToStorage(n, textBox1);
                    else
                        Hopper.RouteChannelToCashbox(n, textBox1);
                }
                catch
                {
                    return;
                }
            }
        }

        public void ConnectToHopper(TextBox log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            Hopper.CommandStructure.ComPort = Global.ComPort;
            Hopper.CommandStructure.SSPAddress = Global.Validator2SSPAddress;
            Hopper.CommandStructure.BaudRate = 9600;
            Hopper.CommandStructure.Timeout = 1000;
            Hopper.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (this.IsDisposed)
                {
                    break;
                }
                if (log != null) log.AppendText("Trying connection to SMART Hopper\r\n");

                // turn encryption off for first stage
                Hopper.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (Hopper.OpenPort() && Hopper.NegotiateKeys(log))
                {
                    Hopper.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxHopperProtocolVersion();
                    if (maxPVersion >= 6)
                        Hopper.SetProtocolVersion(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    Hopper.HopperSetupRequest(log);
                    // Get serial number.
                    Hopper.GetSerialNumber(log);
                    // inhibits, this sets which channels can receive notes
                    Hopper.SetInhibits(log);
                    // set running to true so the hopper begins getting polled
                    hopperRunning = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }

        private byte FindMaxHopperProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                Hopper.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (Hopper.CommandStructure.ResponseData[0] == CCommands.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }

        public void ConnectToNV11(TextBox log = null)
        {
            // setup timer
            System.Windows.Forms.Timer reconnectionTimer = new System.Windows.Forms.Timer();
            reconnectionTimer.Tick += new EventHandler(reconnectionTimer_Tick);
            reconnectionTimer.Interval = 1000; // ms
            int attempts = 10;

            // Setup connection info
            NV11.CommandStructure.ComPort = Global.ComPort;
            NV11.CommandStructure.SSPAddress = Global.Validator1SSPAddress;
            NV11.CommandStructure.BaudRate = 9600;
            NV11.CommandStructure.Timeout = 1000;
            NV11.CommandStructure.RetryLevel = 3;

            // Run for number of attempts specified
            for (int i = 0; i < attempts; i++)
            {
                if (this.IsDisposed)
                {
                    break;
                }
                if (log != null) {
                    if (log.InvokeRequired)
                    {
                        log.Invoke(new MethodInvoker(delegate
                        {
                            log.AppendText("Trying connection to NV11\r\n");
                        }));
                    }
                    else {
                        log.AppendText("Trying connection to NV11\r\n");
                    }
                } 

                // turn encryption off for first stage
                NV11.CommandStructure.EncryptionStatus = false;

                // if the key negotiation is successful then set the rest up
                if (NV11.OpenPort() && NV11.NegotiateKeys(log))
                {
                    NV11.CommandStructure.EncryptionStatus = true; // now encrypting
                    // find the max protocol version this validator supports
                    byte maxPVersion = FindMaxNV11ProtocolVersion();
                    if (maxPVersion >= 6)
                        NV11.SetProtocolVersion(maxPVersion, log);
                    else
                    {
                        MessageBox.Show("This program does not support slaves under protocol 6!", "ERROR");
                        return;
                    }
                    // get info from the validator and store useful vars
                    NV11.ValidatorSetupRequest(log);
                    // Get serial number.
                    NV11.GetSerialNumber(log);
                    // inhibits, this sets which channels can receive notes
                    NV11.SetInhibits(log);
                    // enable payout to begin with
                    NV11.EnablePayout(log);
                    // check for any stored notes
                    NV11.CheckForStoredNotes(log);
                    // report by the 4 byte value of the note, not the channel
                    NV11.SetValueReportingType(false, log);
                    // set running to true so the NV11 begins getting polled
                    NV11Running = true;
                    return;
                }
                // reset timer
                reconnectionTimer.Enabled = true;
                while (reconnectionTimer.Enabled)
                {
                    if (CHelpers.Shutdown)
                        return;
                    Application.DoEvents();
                }
            }
        }

        private void reconnectionTimer_Tick(object sender, EventArgs e)
        {
            if (sender is System.Windows.Forms.Timer)
            {
                System.Windows.Forms.Timer t = sender as System.Windows.Forms.Timer;
                t.Enabled = false;
            }
        }

        private byte FindMaxNV11ProtocolVersion()
        {
            // not dealing with protocol under level 6
            // attempt to set in validator
            byte b = 0x06;
            while (true)
            {
                NV11.SetProtocolVersion(b);
                // If it fails then it can't be set so fall back to previous iteration and return it
                if (NV11.CommandStructure.ResponseData[0] == CCommands.SSP_RESPONSE_FAIL)
                    return --b;
                b++;
                if (b > 20) return 0x06; // return default if p version runs too high
            }
        }

        private void btnResetHopper_Click(object sender, EventArgs e)
        {
            Hopper.Reset(textBox1);
        }

        private void btnHalt_Click(object sender, EventArgs e)
        {
            textBox1.AppendText("Poll loop stopped\r\n");
            hopperRunning = false;
            NV11Running = false;
            btnRun.Enabled = true;
            btnHalt.Enabled = false;
        }

        private void btnSmartEmptyHopper_Click(object sender, EventArgs e)
        {
            Hopper.SmartEmpty(textBox1);
        }

        private void btnResetNoteFloat_Click(object sender, EventArgs e)
        {
            NV11.Reset(textBox1);
        }

        private void btnPayoutByDenom_Click(object sender, EventArgs e)
        {
            if (tbPayout.Text != "" && tbPayoutCurrency.TextLength == 3)
                CalculatePayout(tbPayout.Text, tbPayoutCurrency.Text);
        }

        private void CalculatePayout(string amount, string currency)
        {
            float payoutAmount = 0;

            try
            {
                // Parse amount to a number
                payoutAmount = float.Parse(amount) * 100;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }

            // Now we have a number we have to work out which validator needs to process the payout

            // This section can be modified to payout in any way needed. Currently it pays out one
            // note and the rest from the hopper - this could be extended to payout multiple notes.

            // First work out if we need a note, if it's less than the value of the first channel (assuming channels are in value order)
            // then we know it doesn't need one
            if (payoutAmount < NV11.GetChannelValue(1))
            {
                // So we can send the command straight to the hopper
                Hopper.PayoutAmount((int)payoutAmount, currency.ToCharArray(), textBox1);
            }
            else
            {
                // Otherwise we need to work out how to payout notes
                // Check what value the last note stored in the payout was
                int noteVal = NV11.GetLastNoteValue();
                // If less or equal to payout, pay it out
                if (noteVal <= payoutAmount && noteVal > 0)
                {
                    payoutAmount -= noteVal;// take the note off the total requested
                    NV11.EnablePayout();
                    NV11.PayoutNextNote(textBox1);
                }
                // payout the remaining from the hopper if needed
                if (payoutAmount > 0)
                    Hopper.PayoutAmount((int)payoutAmount, currency.ToCharArray(), textBox1);
            }
        }

        private void timer1_Tick_1(object sender, EventArgs e)
        {
            timer1.Enabled = false;
        }


        private void cbRecycleChannelNV11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbRecycleChannelNV11.Text == "No recycling")
            {
                // switch all notes to stacking by disabling the payout
                NV11.DisablePayout();
            }
            else
            {
                // route all to stack to begin with
                NV11.RouteAllToStack();

                // switch selected note to recycle after enabling payout
                NV11.EnablePayout();
                string name = cbRecycleChannelNV11.Items[cbRecycleChannelNV11.SelectedIndex].ToString();
                string[] sArr = name.Split(' ');
                try
                {
                    NV11.ChangeNoteRoute(Int32.Parse(sArr[0]) * 100, sArr[1].ToCharArray(), false, textBox1);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                    return;
                }
            }
        }

        private void btnStackNextNote_Click(object sender, EventArgs e)
        {
            NV11.StackNextNote(textBox1);
        }

        private void btnPayoutNextNote_Click(object sender, EventArgs e)
        {
            NV11.PayoutNextNote(textBox1);
        }

        private void btnNoteFloatStackAll_Click(object sender, EventArgs e)
        {
           NV11.EmptyPayoutDevice(textBox1);
        }

        private void ReturnNote_Click(object sender, EventArgs e)
        {
            NV11.ReturnNote(textBox1);
        }

        private void btnPayout_Click(object sender, EventArgs e)
        {
            if (tbPayout.Text != "" && tbPayoutCurrency.TextLength == 3)
                CalculatePayout(tbPayout.Text, tbPayoutCurrency.Text);
        }

        private void pictureBox2_Click_1(object sender, EventArgs e)
        {
            Program.formVdo.Close();
        }

        private void txtAmont_TextChanged(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox1_Click_1(object sender, EventArgs e)
        {
            var dialog = new FormLetter();
            Global.ReceiptLetter = false;
            Global.ThankYouLetter = false;
            dialog.ShowDialog();
        }

        private void FormDonate_Shown(object sender, EventArgs e)
        {
           
        }

        private void button12_Click(object sender, EventArgs e)
        {
            NV11.DisablePayout(textBox1);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            NV11.EnablePayout(textBox1);
        }

        private void pictureBox4_Click(object sender, EventArgs e)
        {
            panel2.Visible = false;
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            panel3.Visible = false;
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button5_Click_2(object sender, EventArgs e)
        {
            Thread thread = new Thread(() => runAudio2("SpecifyNumber"));
            thread.Start();
            Program.RefreshTime();
            typeConate = true;
            txtAmont.Text = "";
            OpenPort();
            txtAmont.Enabled = true;
            txtAmont.ReadOnly = false;
            txtAmont.Focus();
            //txtAmont.Text = "0";
            //txtAmount.Text = "0";
        }
    }
}
