﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Donation.WinForm
{
    public partial class FormTypeDoner : Form
    {
        public FormTypeDoner()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Person person = ReadSmartCard.ReadData();
            Global.Person = null;
            if (person.Status.Equals("SUCCESS"))
            {
                Global.Person = person;
                Program.ChangePage(this, Program.formIdentity);
            }
            else {
                MessageBox.Show(person.Status);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Program.formOrganization.Show();
            this.Hide();
        }
    }
}
