﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Media;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    class WAVPlayer
    {
        [DllImport("winmm.dll", SetLastError = true,
                                CallingConvention = CallingConvention.Winapi)]
        static extern bool PlaySound(
            string pszSound,
            IntPtr hMod,
            int sf);


        [Flags]
        public enum SoundFlags : int
        {
            SND_SYNC = 0x0000,  // play synchronously (default) 

            SND_ASYNC = 0x0001,  // play asynchronously 

            SND_NODEFAULT = 0x0002,  // silence (!default) if sound not found 

            SND_MEMORY = 0x0004,  // pszSound points to a memory file

            SND_LOOP = 0x0008,  // loop the sound until next sndPlaySound 

            SND_NOSTOP = 0x0010,  // don't stop any currently playing sound 

            SND_NOWAIT = 0x00002000, // don't wait if the driver is busy 

            SND_ALIAS = 0x00010000, // name is a registry alias 

            SND_ALIAS_ID = 0x00110000, // alias is a predefined ID

            SND_FILENAME = 0x00020000, // name is file name 

            SND_RESOURCE = 0x00040004  // name is resource name or atom 
        }

        public void Play(string wfname)
        {
            int err = 0;
            try
            {
                // play the sound from the selected filename

                if (!PlaySound(wfname, IntPtr.Zero, 0x00020000 | 0x0000 | 0x0002))
                {
                    //Utils.getErrorToLog(":: Unable to find specified sound file or default Windows sound", "WAVPlayer");
                }
            }
            catch
            {
                // grab the underlying Win32 error code

                err = Marshal.GetLastWin32Error();
                if (err != 0)
                {
                    //Utils.getErrorToLog("Error " + err.ToString(), "WAVPlayer");
                    Console.WriteLine("Error " + err.ToString(), "PlaySound() failed");
                }
            }
        }

        public void PlayAudio(string wfname)
        {
            try
            {
                (new SoundPlayer(wfname)).PlaySync();
            }
            catch (Exception ex)
            {
                //Utils.getErrorToLog("Error " + ex.ToString(), "WAVPlayer");
            }
        }
    }
}
