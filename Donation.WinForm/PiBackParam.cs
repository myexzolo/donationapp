﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    public class PiBackParam
    {
        public int coinbaht { get; set; }
        public int bankbaht { get; set; }
        public int total { get; set; }
    }
}
