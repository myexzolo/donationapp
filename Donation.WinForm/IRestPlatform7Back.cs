﻿using Refit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Donation.WinForm
{
    public interface IRestPlatform7Back
    {
        [Post("/piback")]
        Task PiBack([Refit.Body(BodySerializationMethod.Json)]PiBackParam param);
    }
}
