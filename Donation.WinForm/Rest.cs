﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Donation.Interface;
using Donation.Models;
using Donation.WinForm;
using Donation.WinForm.Models;
using Refit;

namespace Donation.Shared
{
    public class Rest
    { 
        IRestPlatform IRestPlatform { get; set; }
        IRestPlatform7Front IRestPlatform7Front { get; set; }
        IRestPlatform7Back IRestPlatform7Back { get; set; }

        public string host = System.Configuration.ConfigurationManager.AppSettings["HOST"];

        
        public Rest()
        {
            string url = "http://" + host + ":9099";
            do
            {
                IRestPlatform = RestService.For<IRestPlatform>(url);
                IRestPlatform7Front = RestService.For<IRestPlatform7Front>("http://192.168.1.45:3000");
                IRestPlatform7Back = RestService.For<IRestPlatform7Back>("http://192.168.1.44:3000");
            }
            while (IRestPlatform == null);
        }

        public async Task<Response<BankAccount>> GetBankAccountFromHomeId(int homeId)
        {
            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetBankAccountFromHomeId(homeId);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<BankAccount>
            {
                data = new System.Collections.Generic.List<BankAccount>
                {
                    new BankAccount()
                }
            };
        }

        public async Task<ResponseForPrint> SummaryFromTo(string token, string fromDate, string toDate)
        {
            token = token ?? "";
            fromDate = fromDate ?? "";
            toDate = toDate ?? "";

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.SummaryFromTo(token,fromDate,toDate);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new ResponseForPrint();
        }


        public async Task<LoginResponse> AdminLogin(string clientName, string password)
        {
            clientName = clientName ?? "";
            password = password ?? "";

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.AdminLogin(clientName, password);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new LoginResponse();
        }

        public async Task<LoginResponse> GuestLogin(string clientName)
        {
            clientName = clientName ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GuestLogin(clientName);
                }
                catch (Exception exc)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new LoginResponse();
        }

        public async Task<ResponseLoginByNationalId> LoginByNationalId(string clientName, string psnId, string firstName, string lastName
          , string homeNo, string mooNo, string soi, string road
          , string provinceName, string amphurName, string tambolName)
        {
            clientName = clientName ?? "";
            psnId = psnId ?? "";
            firstName = firstName ?? "";
            lastName = lastName ?? "";
            homeNo = homeNo ?? "";
            mooNo = mooNo ?? "";
            soi = soi ?? "";
            road = road ?? "";
            provinceName = provinceName ?? "";
            amphurName = amphurName ?? "";
            tambolName = tambolName ?? "";

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.LoginByNationalId(clientName,psnId,firstName
                                                    ,lastName,homeNo,mooNo,soi
                                                    ,road,provinceName,amphurName
                                                   ,tambolName);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new ResponseLoginByNationalId();
        }

        public async Task PiFront(PiFrontParam param)
        {
            int repeat = 100;
            do
            {
                try
                {
                    await IRestPlatform7Front.PiFront(param);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

        }
        public async Task PiBack(PiBackParam param)
        {
            int repeat = 100;
            do
            {
                try
                {
                    await IRestPlatform7Back.PiBack(param);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

        }


        public async Task<Response<Home>> GetHomeList()
        {
            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetHomeList();
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<Home>();
        }

        public async Task<Response<Province>> GetProvinceList()
        {
            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetProvinceList();
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<Province>();

            //return await Service<Response<Province>>(() => IRestPlatform.GetProvinceList());
        }

        public async Task<Response<Amphur>> GetAmphurList(string provinceId)
        {
            provinceId = provinceId ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetAmphurList(provinceId);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<Amphur>();
        }

        public async Task<Response<Tambol>> GetTambolList(string amphurId)
        {
            amphurId = amphurId ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetTambolList(amphurId);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<Tambol>();
        }

        public async Task<Response<VdoUrl>> GetPublicNewsList()
        {
            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetPublicNewsList();
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<VdoUrl>();
        }

        public async Task<Response<MoneyObjective>> GetDonateMoneyObjective(string token)
        {
            token = token ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.GetDonateMoneyObjective(token);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<MoneyObjective>();
        }

        public async Task<Status> Register(RegisterParam param)
        {
            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.Register(param);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Status();
        }

        public async Task<LoginResponse> Login(string email, string password)
        {
            email = email ?? string.Empty;
            password = password ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.Login(email, password);
                }
                catch (Exception exc)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new LoginResponse();
            //return await Service<LoginResponse>(()=>IRestPlatform.Login(email,password));
        }

        public async Task<ForReport> DonateMoney(string token, string money
                                                 , string objective, string note
                                                 , string homeId
                                                 ,int receiptThankyou,int thankyouLetter)
        {
            token = token ?? string.Empty;
            money = money ?? string.Empty;
            objective = objective ?? string.Empty;
            note = note ?? string.Empty;
            homeId = homeId ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.DonateMoney(token, money, objective
                                                           , note,homeId
                                                           ,receiptThankyou,thankyouLetter);
                }
                catch (Exception exc)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new ForReport();
        }

        public async Task<Status> DonateThings(string token,DonateThingsParam param,string homeId)
        {
            token = token ?? string.Empty;
            homeId = homeId ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.DonateThings(token, param,homeId);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Status();
        }

        public async Task<Response<Product>> ProductList(string token)
        {
            token = token ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.ProductList(token);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<Product>();
        }

        public async Task<Response<History>> SearchDonateHistory(string token)
        {
            token = token ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.SearchDonateHistory(token);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<History>();
        }

        public async Task<Response<HistoryItem>> SearchDonateItemHistory
                        (string token, string donationId)
        {
            token = token ?? string.Empty;
            donationId = donationId ?? string.Empty;

            int repeat = 100;
            do
            {
                try
                {
                    return await IRestPlatform.SearchDonateItemHistory
                                              (token, donationId);
                }
                catch (Exception)
                {
                    Thread.Sleep(50);
                }
            }
            while (--repeat > 0);

            return new Response<HistoryItem>();
        }

    }
}
