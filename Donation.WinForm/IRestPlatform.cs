﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Donation.Models;
using Donation.WinForm;
using Donation.WinForm.Models;
using Refit;

namespace Donation.Shared
{
    public interface IRestPlatform
    {
        [Get("/json/kioskService?cmd=listSummaryDonateMoney&token={token}&fromDate={fromDate}&toDate={toDate}")]
        Task<ResponseForPrint> SummaryFromTo(string token,string fromDate,string toDate);

        [Get("/json/kioskService?cmd=authenSuper&clientName={clientName}&password={password}")]
        Task<LoginResponse> AdminLogin(string clientName,string password);

        [Get("/json/kioskService?cmd=authenPsnId&clientName={clientName}&psnId={psnId}&firstName={firstName}&lastName={lastName}&homeNo={homeNo}&mooNo={mooNo}&soi={soi}&road={road}&provinceName={provinceName}&amphurName={amphurName}&tambolName={tambolName}")]
        Task<ResponseLoginByNationalId> LoginByNationalId(string clientName,string psnId,string firstName,string lastName
              ,string homeNo,string mooNo,string soi,string road
              ,string provinceName,string amphurName,string tambolName);
                                              
        [Get("/json/kioskService?cmd=authenGuest&clientName={clientName}")]
        Task<LoginResponse> GuestLogin(string clientName);

        [Get("/json/mobileService?cmd=listDonateToDepartmentUnit")]
        Task<Response<Home>> GetHomeList();

        [Get("/json/mobileService?cmd=listProvince")]
        Task<Response<Province>> GetProvinceList();

        [Get("/json/mobileService?cmd=listAmphur&provinceId={provinceId}")]
        Task<Response<Amphur>> GetAmphurList(string provinceId);

        [Get("/json/mobileService?cmd=listTambol&amphurId={amphurId}")]
        Task<Response<Tambol>> GetTambolList(string amphurId);

        [Get("/json/kioskService?cmd=listPublicNews")]
        Task<Response<VdoUrl>> GetPublicNewsList();

        [Get("/json/kioskService?cmd=listDonateMoneyObjective&token={token}")]
        Task<Response<MoneyObjective>> GetDonateMoneyObjective(string token);

        //[Get("/json/mobileService?cmd=findDonator&psnId={nationalId}&name=")]
        //Task SearchByNationalId(string personalId);

        //[Get("/json/mobileService?cmd=findDonator&psnId=&name={name}")]
        //Task SearchByName(string name);

        [Post("/json/mobileService?cmd=regist")]
        Task<Status> Register([Body(BodySerializationMethod.UrlEncoded)] RegisterParam param);

        [Get("/json/mobileService?cmd=login&login={email}&password={password}")]
        Task<LoginResponse> Login(string email, string password);

        [Get("/json/kioskService?cmd=donateMoney&token={token}&amountMoney={money}&donateMoneyObjective={objective}&donateMoneyNote={note}&donateToDepartmentUnit={homeId}&thankyouMail={nSendLetterThankyou}&moneyReceiptAtPost={nSendLetterReceipt}")]
        Task<ForReport> DonateMoney(string token, string money, string objective, string note, string homeId
                                   ,int nSendLetterReceipt,int nSendLetterThankyou);

        [Post("/json/mobileService?cmd=donateItem&token={token}&donateToDepartmentUnit={homeId}")]
        Task<Status> DonateThings(string token, [Body(BodySerializationMethod.Json)] DonateThingsParam param, string homeId);

        [Get("/json/mobileService?cmd=listItemDonation&token={token}")]
        Task<Response<Product>> ProductList(string token);

        [Get("/json/kioskService?cmd=listDonateHistory&token={token}")]
        Task<Response<History>> SearchDonateHistory(string token);

        [Get("/json/mobileService?cmd=listDonateItemHistory&token={token}&donateId={donationId}")]
        Task<Response<HistoryItem>> SearchDonateItemHistory(string token, string donationId);

        [Get("/json/kioskService?cmd=listBankAccount&departmentUnit={homeId}")]
        Task<Response<BankAccount>> GetBankAccountFromHomeId(int homeId);

    }




}
